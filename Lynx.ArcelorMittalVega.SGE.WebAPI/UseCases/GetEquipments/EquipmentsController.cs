﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.Domain.Equipments;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetEquipments
{
    /// <summary>
    /// Responsável pela consulta de equipamentos no sistema.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class EquipmentsController : ControllerBase
    {
        private readonly IEquipmentQueries _equipmentQueries;
        public EquipmentsController(IEquipmentQueries equipmentQueries, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _equipmentQueries = equipmentQueries;
        }
        /// <summary>
        /// Consulta um equipamento por nome.
        /// </summary>
        /// <param name="equipmentName">Nome do equipamento a ser pesquisado.</param>
        /// <returns></returns>
        [HttpGet("{equipmentName}")]
        public IActionResult Get(string equipmentName)
        {
            try
            {
                LxLogger.Debug("GET Equipment And Variables");

                Equipment equipment = _equipmentQueries.GetEquipment(equipmentName);

                return new ObjectResult(equipment);

            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
               
    }
}
