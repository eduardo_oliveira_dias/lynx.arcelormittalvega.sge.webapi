﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetGraphicsBar
{
    /// <summary>
    /// Responsável por consultar dados estruturados em gráfico de Barra.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class GraphicsController : ControllerBase
    {
        private readonly IGraphicQueries _iGraphicQueries;

        public GraphicsController(IGraphicQueries iGraphicQueries, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _iGraphicQueries = iGraphicQueries;
        }
        /// <summary>
        /// Consulta dados baseados em filtro e estrutura os dados em forma de gráfico de barra.
        /// </summary>
        /// <param name="request">Filtro de pesquisa na base de dados.</param>
        /// <returns>Lista de dados estruturados em gráfico de barra.</returns>
        [HttpPost("groupBar")]
        public ActionResult GetGraphicAlarmsBar([FromBody] RequestGraphicBar request)
        {
            try
            {
                LxLogger.Info($"GET GraphicAlarmsBar startTime = {request.StartTime} | endTime = {request.EndTime} | {request.Level} | {request.LevelName}");
                var list = _iGraphicQueries.GetAlarmsGraphicBar(request.StartTime, request.EndTime, request.Level, request.LevelName);

                return new ObjectResult(list);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
        /// <summary>
        /// Estrutura os dados de gráfico baseado no filtro, sem pesquisar novamente na base de dados.
        /// </summary>
        /// <param name="request">Filtro de pesquisa na base de dados.</param>
        /// <returns>Lista de dados estruturados em gráfico de barra.</returns>
        [HttpPost("mountBar")]
        public ActionResult MountGraphicAlarmsBar([FromBody] RequestGraphicBar request)
        {
            try
            {
                LxLogger.Info($"GET MountGraphicBar startTime = {request.StartTime} | endTime = {request.EndTime}");
                var list = _iGraphicQueries.MountGraphicBar(request.Alarms, request.Level, request.LevelName);

                return new ObjectResult(list);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }

        
    }
}
