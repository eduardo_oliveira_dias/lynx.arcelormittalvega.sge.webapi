﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases
{
    /// <summary>
    /// Responsável por consultar a hierarquia de elementos (FolderDef) na base de dados do IP21.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class PlantLevelsController : Controller
    {
        private readonly IPlantLevelQueries _plantLevelQueries;
        public PlantLevelsController(IPlantLevelQueries plantLevelQueries, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);
            
            _plantLevelQueries = plantLevelQueries;
        }
        /// <summary>
        /// Retorna a hierarquia completa de todos os níveis da hierarquia do SAP View.
        /// </summary>
        /// <returns>Um objeto com a árvore montada.</returns>
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                LxLogger.Debug("GET PlantLevels");

                var plantLevelResult = _plantLevelQueries.GetPlantLevels();

                return new ObjectResult(plantLevelResult);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }

        }
        /// <summary>
        /// Consulta os elementos encontrados em determinado nível da hierarquia por nome ou descrição.
        /// </summary>
        /// <param name="query">Nome do elemento.</param>
        /// <returns>Lista com os elementos encontrados.</returns>
        [HttpGet("{query}")]
        public IActionResult Get(string query)
        {
            try
            {
                LxLogger.Debug("GET Plant Level by query");

                var plantLevelResult = _plantLevelQueries.GetPlantLevels(query);

                return new ObjectResult(plantLevelResult);

            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
        /// <summary>
        /// Consulta os elementos encontrados em determinado nível da hierarquia por nível ou nível pai
        /// </summary>
        /// <param name="levelName">Nome do nível</param>
        /// <param name="parentName">Nome do nível pai.</param>
        /// <returns></returns>
        [HttpGet("level/{levelName}/{parentName?}")]
        public IActionResult GetByLevelName(string levelName, string parentName)
        {
            try
            {
                LxLogger.Debug("GET Plant Level by levelName");

                var plantLevelResult = _plantLevelQueries.GetPlantLevelsByLevelName(levelName, parentName);

                return new ObjectResult(plantLevelResult);

            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }

    }
}
