﻿using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetAlarms
{
    /// <summary>
    /// Classe de filtro para pesquisa de alarmes por timestamp.
    /// </summary>
    public class RequestAlarms
    {
        /// <summary>
        /// Nome da variável do alarme.
        /// </summary>
        public string variableName { get; set; }
        /// <summary>
        /// Data início da pesquisa.
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// Data Fim da pesquisa.
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// Nível dos elementos a serem buscados
        /// </summary>
        public string Level { get; set; }
        /// <summary>
        /// Nome do nível a ser buscado.
        /// </summary>
        public string LevelName { get; set; }
    }
}
