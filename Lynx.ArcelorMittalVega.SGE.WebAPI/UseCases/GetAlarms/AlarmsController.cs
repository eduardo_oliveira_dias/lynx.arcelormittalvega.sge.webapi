﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.Domain.Alarms;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetAlarms
{
    /// <summary>
    /// Responsável por operações de consulta de alarmes da base de dados.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    public class AlarmsController : ControllerBase
    {
        private readonly IAlarmQueries _alarmQueries;

        public AlarmsController(IAlarmQueries alarmQueries, IHttpContextAccessor contextAccessor)
        {
            
            var User = contextAccessor.HttpContext.User;
            if(User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _alarmQueries = alarmQueries;
        }
        /// <summary>
        /// Consulta os alarmes críticos ativos na base de dados.
        /// </summary>
        /// <returns>Lista de alarmes críticos encontrados.</returns>
        [HttpGet("critical")]
        public IActionResult GetCritical()
        {
            try
            {
                LxLogger.Info("GET Critical Alarms");
                List<Alarm> alarms = _alarmQueries.GetCriticalAlarms();
                return new ObjectResult(alarms);

            }
            catch(Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
            
        }
        /// <summary>
        /// Consulta todos os alarmes ativos na base de dados.
        /// </summary>
        /// <returns>Lista de alarmes ativos.</returns>
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                LxLogger.Info("GET ALL Alarms");

                List<Alarm> alarms = _alarmQueries.GetAlarms();

                return new ObjectResult(alarms);
            }
            catch(Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest("Exceção inesperada");
            }
        }
        /// <summary>
        /// Consulta os alarmes ativos de determinado nível.
        /// </summary>
        /// <param name="level">Nível do elemento a ser pesquisado.</param>
        /// <param name="name">Nome do elemento a ser pesquisado.</param>
        /// <returns></returns>
        [HttpGet("{level}/{name}")]
        public IActionResult GetByName(string level, string name)
        {
            try
            {
                LxLogger.Info("GET Alarms By Name");
                
                List<Alarm> alarms = _alarmQueries.GetAlarms(level, name);

                return new ObjectResult(alarms);
            }
            catch (Application.ApplicationException e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
            
        }

        /// <summary>
        /// Consulta a lista de alarmes conforme a pesquisa dentro do tempo.
        /// </summary>
        /// <param name="requestAlarms">Objeto de filtro com os dados de início, fim e variável a ser filtrada.</param>
        /// <returns>Lista de alames encontrados.</returns>
        [HttpPost("trend")]
        public IActionResult GetTrend([FromBody]RequestAlarms requestAlarms)
        {
            try
            {
                LxLogger.Info("GET Alarms By Name");

                List<Alarm> alarms = _alarmQueries.GetAlarmsTrend(requestAlarms.StartTime, requestAlarms.EndTime, requestAlarms.Level, requestAlarms.LevelName);

                return new ObjectResult(alarms);
            }
            catch(Application.ApplicationException e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }

    }
}
