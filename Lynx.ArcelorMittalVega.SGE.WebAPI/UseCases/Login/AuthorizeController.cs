﻿using Lynx.ArcelorMittalVega.SGE.Application.Commands.ManageUsers;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.Domain.Users;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.Login
{
    /// <summary>
    /// Classe <c>AuthorizeController</c>
    /// <para>Classe responsável pelas operações de login no sistema e geração de token de autenticação.</para>
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizeController : ControllerBase
    {
        private readonly IUserLoginUseCase _userLoginUseCase;

        public AuthorizeController(IUserLoginUseCase userLoginUseCase)
        {
            _userLoginUseCase = userLoginUseCase;
        }
        /// <summary>
        /// Método responsável por validar o token que o usuário possui. Método não sendo utilizado
        /// </summary>
        /// <returns>String informando que o token está válido.</returns>
        [Obsolete]
        [HttpGet]
        public ActionResult<string> ValidateToken()
        {
            return "Token validado em " + DateTime.Now.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("login")]
        public ActionResult Login([FromBody] UserAD user)
        {
            try
            {
                LxLogger.Info($"POST Login: {user.Name}");

                var userToken = _userLoginUseCase.Login(user);

                return Ok(userToken);
            }
            catch (Application.ApplicationException e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace + "\n InnerException:" +e.InnerException?.Message+ "\n"+e.InnerException?.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
    }
}
