﻿using Lynx.ArcelorMittalVega.SGE.Domain.Variables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.UpdateVariables
{
    /// <summary>
    /// Classe com a estrutura do Parametro para atualização ou inserção no sistema.
    /// </summary>
    public class VariableParameterRequest
    {
        /// <summary>
        /// Nome do parametro para exibição.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Campo referencia dentro da tag.
        /// </summary>
        public string Field { get; set; }
        /// <summary>
        /// Valor antigo do parametro para atualização.
        /// </summary>
        public object Value { get; set; }
        /// <summary>
        /// Novo valor  do parametro para atualização.
        /// </summary>
        public object NewValue { get; set; }
        /// <summary>
        /// Variável referência que o parametro está localizado.
        /// </summary>
        public Variable Variable { get; set; }
    }
}
