﻿using Lynx.ArcelorMittalVega.SGE.Application.Commands.UpdateVariables;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.UpdateVariables
{
    /// <summary>
    /// Responsável pela atualização de valores de parâmetro da variável.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class VariablesController : ControllerBase
    {
        private readonly IUpdateVariableUseCase _updateVariableUseCase;

        public VariablesController(IUpdateVariableUseCase updateVariableUseCase, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _updateVariableUseCase = updateVariableUseCase;
        }

        /// <summary>
        /// Atualiza o valor do parâmetro da variável.
        /// </summary>
        /// <param name="request">Objeto com o parâmetro a ser atualizado.</param>
        /// <returns>Mensagem de retorno informando se a operação foi bem sucedida.</returns>
        [HttpPost("parameter")]
        public IActionResult UpdateParameter([FromBody]VariableParameterRequest request)
        {
            try
            {
                LxLogger.Debug($"POST Variables/UpdateParameter VariableName: {request.Variable?.Name} | Parameter: {request.Field} | Value: {request.NewValue} oldValue: {request.Value}");

                _updateVariableUseCase.Update(request.Variable?.Name, request.Field, request.NewValue);

                return new ObjectResult(new { Message = MessageUser.SaveSucceed(request.Field) });
            }
            catch (Application.ApplicationException e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
    }
}
