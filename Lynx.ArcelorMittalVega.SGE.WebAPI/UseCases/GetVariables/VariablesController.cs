﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.Domain.Variables;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetVariables
{
    /// <summary>
    /// Responsável pela consulta de variáveis na base de dados.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class VariablesController : ControllerBase
    {
        private readonly IVariableQueries _variableQueries;
        public VariablesController(IVariableQueries variableQueries, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _variableQueries = variableQueries;
        }
        /// <summary>
        /// Consulta as variáveis e tags por nome ou descrição.
        /// </summary>
        /// <param name="query">Nome ou descrição da tag.</param>
        /// <returns>Lista de variáveis que contenham o nome ou descrição passado.</returns>
        [HttpGet("{query}")]
        public IActionResult Get(string query)
        {
            try
            {
                LxLogger.Debug("GET Variables by query");

                List<Variable> variables = _variableQueries.GetVariables(query);

                return new ObjectResult(variables);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
        
    }
}
