﻿using Lynx.ArcelorMittalVega.SGE.Application.Repositories;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.Domain.SystemConfigurations;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetConfigurations
{
    /// <summary>
    /// Responsável pela consulta das configurações do sistema.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigurationsController : ControllerBase
    {
        private readonly IConfigurationReadOnlyRepository _configurationReadOnlyRepository;

        public ConfigurationsController(IConfigurationReadOnlyRepository configurationReadOnlyRepository)
        {
            _configurationReadOnlyRepository = configurationReadOnlyRepository;
        }
        /// <summary>
        /// Consulta a configuração pela chave passada.
        /// </summary>
        /// <param name="key">Chave da configuração.</param>
        /// <returns>Objeto chave-valor com a configuração encontrada.</returns>
        [HttpGet("{key}")]
        public IActionResult Get(string key)
        {
            var obj = _configurationReadOnlyRepository.Get(key);

            return new ObjectResult(obj);
        }

        /// <summary>
        /// Consulta todas as configurações definidads como gerais (Record: SGE_Config)
        /// </summary>
        /// <returns>Lista de objetos chave-valor com as configurações encontradas</returns>
        [HttpGet("general")]
        public IActionResult GetGeneral()
        {
            try
            {
                LxLogger.Info("GET General Configurations");

                List<SystemConfiguration> systemConfigurations = _configurationReadOnlyRepository.GetGeneralConfigurations();

                return new ObjectResult(systemConfigurations);

            }catch(Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }

        /// <summary>
        /// Consulta as configurações de posições dos items de gráfico da tela de equipamentos.
        /// </summary>
        /// <returns>Lista de objetos chave-valor com as posições encontradas.</returns>
        [HttpGet("itemPositions")]
        public IActionResult GetItemPositions()
        {
            try
            {
                LxLogger.Info("GET Item Positions");

                List<SystemConfiguration> systemconfigurations = _configurationReadOnlyRepository.GetObjectPositions();

                return new ObjectResult(systemconfigurations);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }

        /// <summary>
        /// Consulta os parametros de variáveis a serem listados na tela do equipamento.
        /// </summary>
        /// <returns>Lista de objetos chave-valor com os parametros encontrados.</returns>
        [HttpGet("VariableParameters")]
        public IActionResult GetVariableParameters()
        {
            try
            {
                LxLogger.Info("GET Variable Parameters");

                List<SystemConfiguration> systemconfigurations = _configurationReadOnlyRepository.GetVariableParameters();

                return new ObjectResult(systemconfigurations);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
    }
}
