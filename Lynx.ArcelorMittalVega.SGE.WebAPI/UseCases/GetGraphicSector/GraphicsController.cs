﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetGraphicSector
{
    /// <summary>
    /// Responsável pela consulta de gráficos de setor, ou pizza.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class GraphicsController : ControllerBase
    {
        private readonly IGraphicQueries _iGraphicQueries;

        public GraphicsController(IGraphicQueries iGraphicQueries, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _iGraphicQueries = iGraphicQueries;
        }
        /// <summary>
        /// Consulta os alarmes por nível da hierarquia (elemento) para estruturar os gráficos de pizza por segmento, área, seção e equipamentos.
        /// </summary>
        /// <param name="childrenLevel">Nível dos Filhos do elemento</param>
        /// <param name="parentLevel">Nível do elemento pesquisado</param>
        /// <param name="parentLevelName">Nome do elemento(CÓDIGO SAP)</param>
        /// <returns>Lista de objetos com dados estruturados para gráfico de Setor.</returns>
        [HttpGet("sector/{childrenLevel=segmentName}/{parentLevel=root}/{parentLevelName=BV}")]
        public ActionResult GetSector(string childrenLevel, string parentLevel, string parentLevelName)
        {
            try 
            {
                LxLogger.Info($"GET GraphicSector childrenLevel = {childrenLevel} | parentLevel = {parentLevel} | parentLevelName = {parentLevelName}");
                var list = _iGraphicQueries.GetAlarmsGraphicSector(childrenLevel, parentLevel, parentLevelName);

                return new ObjectResult(list);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
            
        }
        /// <summary>
        /// Consulta os alarmes por tipo e estrutura no formato pizza.
        /// </summary>
        /// <param name="request">Objeto com o Filtro de pesquisa de alarmes</param>
        /// <returns>Objeto com dados estruturados para gráfico de Setor.</returns>
        [HttpPost]
        public ActionResult GetGraphicAlarmsByType([FromBody] RequestGraphicSector request)
        {
            try
            {
                LxLogger.Info($"GET GraphicAlarmsByType startTime = {request.StartTime} | endTime = {request.EndTime}");
                var list = _iGraphicQueries.GetAlarmsGraphicType(request.StartTime, request.EndTime, request.Level, request.LevelName);

                return new ObjectResult(list);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
        /// <summary>
        /// Monta os dados estruturados em formato de gráfico de pizza a partir da lista de alarmes.
        /// </summary>
        /// <param name="request">Objeto com a lista de alarmes.</param>
        /// <returns>Objeto com dados estruturados para gráfico de Setor.</returns>
        [HttpPost("mountType")]
        public ActionResult MountGraphicAlarmsBar([FromBody] RequestGraphicSector request)
        {
            try
            {
                LxLogger.Info($"GET MountGraphicType startTime = {request.StartTime} | endTime = {request.EndTime}");
                var list = _iGraphicQueries.MountGraphicType(request.Alarms, request.Level, request.LevelName);

                return new ObjectResult(list);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
    }
}
