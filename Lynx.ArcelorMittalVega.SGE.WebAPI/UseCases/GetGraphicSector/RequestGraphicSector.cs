﻿using Lynx.ArcelorMittalVega.SGE.Domain.Alarms;
using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetGraphicSector
{
    /// <summary>
    /// Classe que estrutura o filtro de alarmes para consultas de gráfico de setor(pizza).
    /// </summary>
    public class RequestGraphicSector
    {
        /// <summary>
        /// Data início da pesquisa.
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// Data fim da pesquisa
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// Nível dos elementos a serem buscados
        /// </summary>
        public Level Level { get; set; }
        /// <summary>
        /// Nome do nível a ser buscado.
        /// </summary>
        public string LevelName { get; set; }
        /// <summary>
        /// Lista de alarmes para montagem do gráfico.
        /// </summary>
        public List<Alarm> Alarms { get; set; } = new List<Alarm>();
        public RequestGraphicSector()
        {
            Level = Level.Plant;
        }
    }
}
