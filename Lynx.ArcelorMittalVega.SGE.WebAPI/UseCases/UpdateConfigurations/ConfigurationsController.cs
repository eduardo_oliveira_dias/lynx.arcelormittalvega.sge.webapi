﻿using Lynx.ArcelorMittalVega.SGE.Application.Commands.UpdateConfigurations;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.Domain.SystemConfigurations;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.UpdateConfigurations
{
    /// <summary>
    /// Reponsável pela consulta de configurações do sistema.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigurationsController : ControllerBase
    {
        private readonly IUpdateConfigurationsUseCase _updateConfigurationsUseCase;

        public ConfigurationsController(IUpdateConfigurationsUseCase updateConfigurationsUseCase, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _updateConfigurationsUseCase = updateConfigurationsUseCase;
        }
        /// <summary>
        /// Atualiza o valor de uma ou mais configurações gerais do sistema.
        /// </summary>
        /// <param name="request">Objeto com as configurações a serem atualizadas.</param>
        /// <returns>Mensagem de retorno informando se a operação foi bem sucedida.</returns>
        [HttpPut("general")]
        public IActionResult UpdateGeneral([FromBody] RequestUpdateConfigurations request)
        {
            try
            {
                LxLogger.Debug($"PUT Update general configurations");

                _updateConfigurationsUseCase.UpdateGeneralConfiguration(request.Configurations);

                return new ObjectResult(new { Message = MessageUser.SaveSucceed("Configurações") });
            }
            catch (Application.ApplicationException e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
        /// <summary>
        /// Remove um parametro de variável configurado.
        /// </summary>
        /// <param name="key">Nome do parâmetro a ser removido.</param>
        /// <returns>Mensagem de retorno informando se a operação foi bem sucedida.</returns>
        [HttpDelete("variableParameters/{key}")]
        public IActionResult DeleteVariableParameters(string key)
        {
            try
            {
                LxLogger.Debug($"DELETE Variable Parameter: {key}");

                _updateConfigurationsUseCase.DeleteVariableParameter(key);

                return new ObjectResult(new { Message = MessageUser.DeleteSucceed("Parâmetro") });
            }
            catch (Application.ApplicationException e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
        /// <summary>
        /// Insere um novo parâmetro de variável a ser gerido pelo sistema.
        /// </summary>
        /// <param name="parameter">Objeto chave-valor com o nome e a descrição do parâmetro.</param>
        /// <returns>Mensagem de retorno informando se a operação foi bem sucedida.</returns>
        [HttpPost("variableParameters")]
        public IActionResult InsertVariableParameters(SystemConfiguration parameter)
        {
            try
            {
                LxLogger.Debug($"POST Insert Variable Parameter: {parameter.Key}");

                _updateConfigurationsUseCase.AddVariableParameter(parameter);

                return new ObjectResult(new { Message = MessageUser.SaveSucceed("Parâmetro") });
            }
            catch (Application.ApplicationException e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
    }
}
