﻿using Lynx.ArcelorMittalVega.SGE.Domain.SystemConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.UpdateConfigurations
{
    /// <summary>
    /// Classe com as configurações a serem atualizadas no sistema.
    /// </summary>
    public class RequestUpdateConfigurations
    {
        public RequestUpdateConfigurations()
        {
            Configurations = new List<SystemConfiguration>();
        }
        /// <summary>
        /// Lista de configurações a serem atualizadas.
        /// </summary>
        public List<SystemConfiguration> Configurations { get; set; }
    }
}
