﻿using Lynx.ArcelorMittalVega.SGE.Application.Commands.UploadImages;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetImages
{
    /// <summary>
    /// Responsável pela consulta das imagens no servidor para apresentação no frontend.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        public static IWebHostEnvironment _environment;
        private readonly IUploadImageUseCase _iUploadImageUseCase;
        public ImagesController(IWebHostEnvironment environment, IUploadImageUseCase iUploadImageUseCase, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _environment = environment;
            _iUploadImageUseCase = iUploadImageUseCase;
        }

        /// <summary>
        /// Consulta uma imagem por nome do elemento.
        /// </summary>
        /// <param name="name">Nome do elemento da árvore(CÓDIGO SAP)</param>
        /// <returns>Um objeto com o conteúdo binário da imagem.</returns>
        [HttpGet("{name}")]
        public ActionResult Get(string name)
        {
            try
            {
                LxLogger.Debug("GET Image By Name");
                Byte[] imageBytes;
                if (System.IO.File.Exists(_environment.WebRootPath + "\\images\\" + name + ".jpg"))
                {
                    imageBytes = System.IO.File.ReadAllBytes(_environment.WebRootPath + "\\images\\" + name + ".jpg");

                    return new ObjectResult(new ImageResult { Type = "jpg", Content = imageBytes });
                }
                else if (System.IO.File.Exists(_environment.WebRootPath + "\\images\\" + name + ".png"))
                {
                    imageBytes = System.IO.File.ReadAllBytes(_environment.WebRootPath + "\\images\\" + name + ".png");

                    return new ObjectResult(new ImageResult { Type = "png", Content = imageBytes });
                }
                else
                {
                    throw new Exception($"Imagem do item {name} não encontrada.");
                }
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// Consulta todas as imagens salvas no servidor.
        /// </summary>
        /// <returns>Lista de arquivos binários de imagens. </returns>
        [HttpGet]
        public ActionResult GetImages()
        {
            try
            {
                string[] filePaths = null;
                List<Image> images = new List<Image>();
                string path = _environment.WebRootPath + "\\images\\";

                if (Directory.Exists(path))
                {
                    filePaths = Directory.GetFiles(path);
                    filePaths = filePaths.Where(f => f.Contains(".jpg") || f.Contains(".png")).Select(f => f.Replace(path, "")).ToArray();

                    images.AddRange(filePaths.OrderBy(x =>x).Select(filePath => new Image { Name = filePath.Replace(".jpg", "").Replace(".png",""), Url = filePath }));
                }

                return new ObjectResult(images);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }

        }
    }
}
