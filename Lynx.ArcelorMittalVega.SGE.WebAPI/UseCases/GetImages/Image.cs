﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetImages
{
    /// <summary>
    /// Classe que estrutura os dados de image para upload
    /// </summary>
    public class Image
    {
        /// <summary>
        /// Nome do elemento que a imagem será inserida.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Caminho da imagem no servidor.
        /// </summary>
        public string Url { get; set; }
    }
}
