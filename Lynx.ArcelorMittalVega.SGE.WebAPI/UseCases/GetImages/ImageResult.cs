﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetImages
{
    /// <summary>
    /// Classe que estrutura o tipo da imagem e o conteúdo dela.
    /// </summary>
    public class ImageResult
    {
        /// <summary>
        /// Tipo da imagem.
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Conteúdp da imagem em binário.
        /// </summary>
        public Byte[] Content { get; set; }
    }
}
