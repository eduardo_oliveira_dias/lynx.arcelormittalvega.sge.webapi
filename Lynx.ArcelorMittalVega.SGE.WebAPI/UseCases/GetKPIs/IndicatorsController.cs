﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.Domain.Indicators;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetKPIs
{
    /// <summary>
    /// Responsável pela consulta de indicadores e kpis na base de dados.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class IndicatorsController : ControllerBase
    {
        private readonly IIndicatorQueries _indicatorQueries;
        public IndicatorsController(IIndicatorQueries indicatorQueries, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _indicatorQueries = indicatorQueries;
        }
        /// <summary>
        /// Consulta o valor atual do kpi do elemento selecionado.
        /// </summary>
        /// <param name="itemName">Nome do elemento(CÓDIGO SAP)</param>
        /// <returns>Um objeto com o valor do kpi.</returns>
        [HttpGet("kpi/{itemName}")]
        public IActionResult GetKPI(string itemName) {
            try
            {
                LxLogger.Info($"GET GetKPI {itemName}");

                Indicator indicator = _indicatorQueries.GetKPI(itemName);

                return new ObjectResult(indicator);
            }
            catch(Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
            
        }
        /// <summary>
        /// Consulta os valores do kpi baseado numa busca por nome e data.
        /// </summary>
        /// <param name="request">Um objeto de filtro com o período de busca e o elemento a ser consultado o kpi.</param>
        /// <returns>Lista de objeto com valores de kpi encontrados.</returns>
        [HttpPost]
        public IActionResult GetKPI([FromBody] RequestKPI request)
        {
            try
            {
                LxLogger.Info($"GET GetKPIs {request.StartTime} - {request.EndTime} | {request.Level} ");

                List<Indicator> indicators = _indicatorQueries.GetKPI(request.StartTime, request.EndTime, request.Level, request.LevelName);

                return new ObjectResult(indicators);

            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
    }
}
