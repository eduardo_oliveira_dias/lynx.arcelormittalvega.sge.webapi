﻿using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetKPIs
{
    /// <summary>
    /// Classe que estrutura o filtro para consulta de kpis na base de dados.
    /// </summary>
    public class RequestKPI
    {
        /// <summary>
        /// Data início da busca.
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// Data fim da busca.
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// Nível do elemento a ser buscado.
        /// </summary>
        public Level Level { get; set; }
        /// <summary>
        /// Nome do elemento a ser buscado(CÓDIGO SAP)
        /// </summary>
        public string LevelName { get; set; }

        public RequestKPI()
        {
            Level = Level.Segment;
        }
    }
}
