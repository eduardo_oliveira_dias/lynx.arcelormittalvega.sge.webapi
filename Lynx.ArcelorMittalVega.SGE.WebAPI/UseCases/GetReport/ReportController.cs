﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.Domain.Reports;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetReport
{
    /// <summary>
    /// Responsável por fazer as consultas de relatório e gráficos.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly IReportQueries _reportQueries;

        public ReportController(IReportQueries reportQueries, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _reportQueries = reportQueries;
        }
        /// <summary>
        /// Consulta os alarmes dos equipamentos e monta os gráficos de barra.
        /// </summary>
        /// <param name="filter">Objeto com o filtro da busca.</param>
        /// <returns>Lista com os valores estruturados em gráficos de barra.</returns>
        [HttpPost("report/graphicBar")]
        public ActionResult GetGraphicBarEquipments([FromBody] ReportFilter filter)
        {
            try
            {
                var graphicData = _reportQueries.GetGraphicBar(filter);

                return new ObjectResult(graphicData);

            } catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
        /// <summary>
        /// Consulta os alarmes dos equipamentos para listagem de equipamentos e seus status.
        /// </summary>
        /// <param name="filter">Objeto com o filtro da busca.</param>
        /// <returns>Lista de equipamentos e seus respectivos status.</returns>
        [HttpPost("report/equipmentStatus")]
        public ActionResult GetEquipmentStatus([FromBody] ReportFilter filter)
        {
            try
            {
                var listEquipments = _reportQueries.GetEquipmentStatus(filter);

                return new ObjectResult(listEquipments);

            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
        /// <summary>
        /// Consulta os dados de kpis das áreas, conforme o filtro.
        /// </summary>
        /// <param name="filter">Objeto com o filtro da busca.</param>
        /// <returns>Lista de valores de indicadores kpi para montagem do gráfico de tendência.</returns>
        [HttpPost("report/graphicTrend")]
        public ActionResult GetGraphicTrendKPI([FromBody] ReportFilter filter)
        {
            try
            {
                var graphicData = _reportQueries.GetGraphicTrendKPI(filter);

                return new ObjectResult(graphicData);

            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
        /// <summary>
        /// Consulta os dados de equipamentos e kpis, retornando o relatório de equipamentos com os gráficos e a listagem de equipamentos.
        /// </summary>
        /// <param name="reportFilter">Objeto com o filtro da busca.</param>
        /// <returns>Objeto com o relatório.</returns>
        [HttpPost("equipments")]
        public ActionResult GetReportEquipments([FromBody] ReportFilter reportFilter)
        {
            try
            {
                LxLogger.Debug($"POST Reports startTime:{reportFilter.StartTime} endTime:{reportFilter.EndTime}");
                var reportEquipments = _reportQueries.GetReportEquipments(reportFilter);

                return new ObjectResult(reportEquipments);

            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
    }
}
