﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.UploadImages
{
    /// <summary>
    /// Objeto com a imagem a ser atualizada no sistema.
    /// </summary>
    public class ImageRequest
    {
        /// <summary>
        /// Arquivo a ser inserido na pasta.
        /// </summary>
        public IFormFile file { get; set; }
        /// <summary>
        /// Nome da imagem a ser inserida.
        /// </summary>
        public string Name { get; set; }
    }
}
