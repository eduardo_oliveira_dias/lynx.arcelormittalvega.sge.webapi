﻿using Lynx.ArcelorMittalVega.SGE.Application.Commands.UploadImages;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.UploadImages
{
    /// <summary>
    /// Responsável por atualizar e remover as imagens do sistema.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    public class ImagesController : ControllerBase
    {
        public static IWebHostEnvironment _environment;
        private readonly IUploadImageUseCase _iUploadImageUseCase;
        public ImagesController(IWebHostEnvironment environment, IUploadImageUseCase iUploadImageUseCase, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _environment = environment;
            _iUploadImageUseCase = iUploadImageUseCase;
        }
        /// <summary>
        /// Atualiza uma imagem enviada.
        /// </summary>
        /// <param name="request">Objeto com a imagem a ser atualizada.</param>
        /// <returns>Mensagem de retorno informando se a operação foi bem sucedida.</returns>
        [HttpPost, DisableRequestSizeLimit]
        public ActionResult Upload(ImageRequest request)
        {
            LxLogger.Debug("POST UploadImage");
            if (request.file?.Length > 0)
            {
                try
                {
                    LxLogger.Debug($"NAME: {request.Name} | FILE SENT {request.file.FileName} LENGTH: {request.file.Length}");
                    string[] extensions = new string[] { ".jpg", ".png" };
                    if (!extensions.Contains(Path.GetExtension(request.file.FileName)))
                    {
                        throw new Application.ApplicationException(MessageUser.InvalidFileFormat());
                    }
                    string path = _environment.WebRootPath + "\\images\\";
                    string nameFile = request.Name + "." + request.file.FileName.Split('.').Last();

                    string retorno = _iUploadImageUseCase.Execute(request.file, path, nameFile);
                        
                    return new ObjectResult(new { Message = MessageUser.SaveSucceed($"Imagem de {request.Name} ")});
                    
                }
                catch (Application.ApplicationException e)
                {
                    LxLogger.Error(e.Message + " \n" + e.StackTrace);
                    return BadRequest( e.Message );
                }
                catch (Exception e)
                {
                    LxLogger.Error(e.Message + " \n" + e.StackTrace);
                    return BadRequest( e.Message );

                }
            }
            else
            {
                LxLogger.Error("File Length 0");
                return BadRequest( MessageUser.UnexpectedException());
            }
            
        }
        /// <summary>
        /// Remove uma imagem a partir do nome.
        /// </summary>
        /// <param name="name">O nome do arquivo a ser excluído.</param>
        /// <returns></returns>
        [HttpDelete("{name}")]
        public ActionResult DeleteFile(string name)
        {
            try
            {
                string path = _environment.WebRootPath + "\\images\\";
                string[] filesInDirectory = Directory.GetFiles(path).Where(x => x.Contains(name+".jpg") || x.Contains(name + ".png")).ToArray();

                if(filesInDirectory.Length > 0)
                {
                    foreach(string fileFound in filesInDirectory)
                    {
                        System.IO.File.Delete(fileFound);
                    }
                }
                else
                {
                    throw new Application.ApplicationException(MessageUser.FileNotFound());
                }

                return new ObjectResult(new { Message = MessageUser.DeleteSucceed($"Imagem de {name} ") });
            }
            catch (Application.ApplicationException e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);

            }
        }
        
    }
}
