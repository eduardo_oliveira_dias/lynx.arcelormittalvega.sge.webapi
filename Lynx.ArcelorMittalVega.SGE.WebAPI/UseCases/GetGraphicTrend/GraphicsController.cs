﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Domain.Graphics;
using Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetGraphicTrend
{
    /// <summary>
    /// Responsável pela consulta de gráficos de tendência para valores de tags no tempo.
    /// </summary>
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    public class GraphicsController : ControllerBase
    {
        private readonly IGraphicQueries _graphicQueries;
        public GraphicsController(IGraphicQueries graphicQueries, IHttpContextAccessor contextAccessor)
        {
            var User = contextAccessor.HttpContext.User;
            if (User != null) LxLogger.Info("Usuário: " + User.Identity.Name);

            _graphicQueries = graphicQueries;
        }
        /// <summary>
        /// Consulta os valores das tags para estruturar gráfico de tendência por data.
        /// </summary>
        /// <param name="requestGraphic">Objeto com os filtros de data e nome das variáveis a serem plotadas.</param>
        /// <returns>Lista de dados estruturados para gráfico de linhas.</returns>
        [HttpPost("getTrend")]
        public IActionResult GetGraphicTrendByRequest([FromBody] RequestGraphicTrend requestGraphic)
        {
            try
            {
                LxLogger.Info($"POST GetGraphicTrend By Request Name: {requestGraphic.variableNames.ToString()} | StartTime: {requestGraphic.StartTime} | EndTime: {requestGraphic.EndTime}");

                List<GraphicTrend> graphicTrendList = _graphicQueries.GetAlarmsGraphicTrend(requestGraphic.variableNames, requestGraphic.StartTime, requestGraphic.EndTime).ToList();

                return new ObjectResult(graphicTrendList);
            }
            catch(Application.ApplicationException e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                LxLogger.Error(e.Message + " \n" + e.StackTrace);
                return BadRequest(MessageUser.UnexpectedException());
            }
        }
    }
}
