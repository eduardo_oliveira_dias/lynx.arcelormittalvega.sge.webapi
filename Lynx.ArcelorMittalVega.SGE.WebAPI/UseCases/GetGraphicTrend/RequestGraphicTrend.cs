﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.UseCases.GetGraphicTrend
{
    /// <summary>
    /// Classe de filtro para busca de valores de tags por período, para estruturação de gráfico de tendência.
    /// </summary>
    public class RequestGraphicTrend
    {
        /// <summary>
        /// Lista com os nomes das variáveis para pesquisa.
        /// </summary>
        public List<string> variableNames { get; set; } = new List<string>();
        /// <summary>
        /// Data início da pesquisa.
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// Data fim da pesquisa.
        /// </summary>
        public DateTime EndTime { get; set; }

    }
}
