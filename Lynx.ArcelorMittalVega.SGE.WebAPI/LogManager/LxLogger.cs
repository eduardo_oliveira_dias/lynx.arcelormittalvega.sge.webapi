﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;

namespace Lynx.ArcelorMittalVega.SGE.WebAPI.LogManager
{
    /// <summary>
    /// Classe responsável por salvar o arquivo de log do sistema.
    /// </summary>
    public static class LxLogger
    {

        private static readonly string LOG_CONFIG_FILE = @"log4net.config";

        private static readonly log4net.ILog _log = GetLogger(typeof(LxLogger));

        public static ILog GetLogger(Type type)
        {
            return log4net.LogManager.GetLogger(type);
        }
        /// <summary>
        /// Gera um log do nível Debug.
        /// </summary>
        /// <param name="message">Mensagem a ser escrita no log.</param>
        public static void Debug(object message)
        {
            SetLog4NetConfiguration();
            _log.Debug(message);
        }
        /// <summary>
        /// Gera um log de Erro.
        /// </summary>
        /// <param name="message">Mensagem a ser escrita no log.</param>
        public static void Error(object message)
        {
            SetLog4NetConfiguration();
            _log.Error(message);
        }
        /// <summary>
        /// Gera um log de Erro Fatal.
        /// </summary>
        /// <param name="message"></param>
        public static void Fatal(object message)
        {
            SetLog4NetConfiguration();
            _log.Fatal(message);
        }
        /// <summary>
        /// Gera um log de informação.
        /// </summary>
        /// <param name="message"></param>
        public static void Info(object message)
        {
            SetLog4NetConfiguration();
            _log.Info(message);
        }
        /// <summary>
        /// Realiza a configuração do log.
        /// </summary>
        private static void SetLog4NetConfiguration()
        {
            
            string filePath = Startup.Environment.ContentRootPath +Path.DirectorySeparatorChar+ LOG_CONFIG_FILE;
            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead(filePath));

            var repo = log4net.LogManager.CreateRepository(
                Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
        }
    }
}
