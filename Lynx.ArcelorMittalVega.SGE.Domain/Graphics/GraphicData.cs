﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.Graphics
{
    /// <summary>
    /// Classe que estrutura os dados para apresentação de gráficos.
    /// </summary>
    public class GraphicData
    {
        public string Info { get; set; }
        public string Description { get; set; }
        public List<GraphicDataItem> Items { get; set; } = new List<GraphicDataItem>();

        public override int GetHashCode()
        {
            return Info.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (!(obj is GraphicData))
                return false;
            GraphicData other = (GraphicData)obj;

            return other.Info.Equals(Info);
        }
    }
}
