﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.Graphics
{
    public class GraphicDataItem
    {
        public string Name { get; set; }

        private double _value;
        private double _value2;

        //Os getters and setters da classe irão tentar converter o valor passado para double automaticamente.
        public object Value { 
            get {
                return _value; }
            set {
                Double.TryParse(value.ToString(), out _value);
            } }

        public object Value2 {
            get
            {
                return _value2;
            }
            set
            {
                Double.TryParse(value.ToString(), out _value2);
            }
        }
    }
}
