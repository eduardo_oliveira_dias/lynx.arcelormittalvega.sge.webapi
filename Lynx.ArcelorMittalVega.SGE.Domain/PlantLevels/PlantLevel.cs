﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels
{
    /// <summary>
    /// Classe que estrutura os dados do folder como nível da planta.
    /// </summary>
    public class PlantLevel
    {
        /// <summary>
        /// Nome do elemento (CÓDIGO SAP).
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Descrição do elemento.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Nome do nível que o elemento está.
        /// </summary>
        public string Level { get; set; }
        /// <summary>
        /// Pai do elemento.
        /// </summary>
        public PlantLevel Parent { get; set; }
        /// <summary>
        /// Lista de filhos do elemento.
        /// </summary>
        public List<PlantLevel> Children { get; set; } = new List<PlantLevel>();
        /// <summary>
        /// Contagem de alarmes ativos para esse elemento.
        /// </summary>
        public int AlarmCount { get; set; }
        /// <summary>
        /// Nome do pai do elemento.
        /// </summary>
        public string ParentName { get; set; }

        public PlantLevel()
        {

        }
        /// <summary>
        /// Contagem total de alarmes do elemento somados aos alarmes dos filhos.
        /// </summary>
        public int TotalAlarms { get { return AlarmCount + Children.Sum(x => x.TotalAlarms); } }

        public static PlantLevel Load(string name, string description)
        {
            PlantLevel plantLevel = new PlantLevel();
            plantLevel.Name = name;
            plantLevel.Description = description ?? name;

            return plantLevel;
        }

        /// <summary>
        /// Monta cada nível da hierarquia a partir dos parâmetros
        /// </summary>
        /// <param name="name">Nome do nível, ou código SAP</param>
        /// <param name="description">Descrição do nível, o nome descritivo daquele nível</param>
        /// <param name="level">Nível que a pasta está na árvore, baseado na quantidade de caracteres do código SAP</param>
        /// <param name="alarmCount">Quantidade de alarmes vindos da base de dados, se não tiver</param>
        /// <param name="parentName"></param>
        /// <returns></returns>
        public static PlantLevel Load(string name, string description, string level, int alarmCount, string parentName)
        {
            PlantLevel plantLevel = new PlantLevel();
            plantLevel.Name = name;
            plantLevel.Description = description;
            plantLevel.Level = level;
            plantLevel.AlarmCount = alarmCount;
            plantLevel.ParentName = parentName;

            return plantLevel;
        }

        /// <summary>
        /// Função responsável por montar a árvore de pastas vindas da base de dados do IP21
        /// </summary>
        /// <param name="plantLevels">A lista de níveis da planta(pastas) a ser montada a hierarquia</param>
        /// <returns></returns>
        public static PlantLevel LoadTree(List<PlantLevel> plantLevels)
        {
            PlantLevel root = plantLevels.FirstOrDefault(plantLevel => plantLevel.Name == "BV");
            root.Children = plantLevels.Where(pl => pl.ParentName == root.Name).ToList();
            GetChildren(root.Children, plantLevels);

            return root;
        }
        
        /// <summary>
        /// Função recursiva responsável por identificar e atribuir as pastas filhas de cada pasta.
        /// </summary>
        /// <param name="listChildren">Filhos da pasta atual</param>
        /// <param name="listAll">Todas as pastas</param>
        public static void GetChildren(List<PlantLevel> listChildren, List<PlantLevel> listAll)
        {
            foreach(PlantLevel plantLevel in listChildren)
            {
                plantLevel.Children = listAll.Where(pl => pl.ParentName == plantLevel.Name).ToList();
                GetChildren(plantLevel.Children, listAll);
            }
        }
    }
}
