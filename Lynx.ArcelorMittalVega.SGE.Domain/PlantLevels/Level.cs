﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels
{
    /// <summary>
    /// Enum que parametriza o nível que o elemento está
    /// </summary>
    public enum Level
    {
        Plant = 1,
        Segment = 2,
        Area = 3,
        Section = 4,
        Equipment = 5
    }
}
