﻿using Lynx.ArcelorMittalVega.SGE.Domain.Alarms.Enums;
using Lynx.ArcelorMittalVega.SGE.Domain.Variables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.Equipments
{
    /// <summary>
    /// Classe responsável por estruturar os dados de equipamento
    /// </summary>
    public class Equipment
    {
        /// <summary>
        /// Nome do equipamento (CÓDIGO SAP).
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Descrição do equipamento.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Lista de variáveis do equipamento.
        /// </summary>
        public List<Variable> Variables { get;set; }
        /// <summary>
        /// Área do equipamento(CÓDIGO SAP).
        /// </summary>
        public string AreaName { get; set; }
        /// <summary>
        /// Descrição da área.
        /// </summary>
        public string AreaDescription { get; set; }
        /// <summary>
        /// Nome da seção do equipamento(CÓDIGO SAP).
        /// </summary>
        public string SectionName { get; set; }
        /// <summary>
        /// Descrição da seção.
        /// </summary>
        public string SectionDescription { get; set; }
        /// <summary>
        /// Nome do segmento do equipamento(CÓDIGO SAP).
        /// </summary>
        public string SegmentName { get; set; }
        /// <summary>
        /// Descrição do segmento.
        /// </summary>
        public string SegmentDescription { get; set; }
        /// <summary>
        /// Status do equipamento.
        /// </summary>
        public string SeverityStatus { get; set; }
        /// <summary>
        /// Timestamp do status do equipamento.
        /// </summary>
        public DateTime TimeStampSeverityStatus { get; set; }
        /// <summary>
        /// Valor percentual do período que o equipamento esteve CRITICO.
        /// </summary>
        public double? CriticalStatusPeriod { get; set; }
        /// <summary>
        /// Valor percentual do período que o equipamento esteve ALERTA.
        /// </summary>
        public double? AlertStatusPeriod { get; set; }
        /// <summary>
        /// Tempo que o equipamento esteve no status atual.
        /// </summary>
        public TimeSpan? SeverityStatusDuration { get; set; }
        /// <summary>
        /// Visualização amigável para o tempo que o equipamento esteve no status atual.
        /// </summary>
        public string SeverityStatusDurationView { get { 
                if(SeverityStatusDuration?.TotalHours > 24)
                {
                    return SeverityStatusDuration?.TotalDays.ToString("F2")+" Dias";
                }else if(SeverityStatusDuration?.TotalHours <= 24 && SeverityStatusDuration?.TotalMinutes > 60)
                {
                    return SeverityStatusDuration?.TotalHours.ToString("F2") + " horas";
                }
                else if(SeverityStatusDuration != null)
                {
                    return SeverityStatusDuration?.TotalMinutes.ToString("F2") + " minutos";
                }
                else
                {
                    return "-";
                }
            } }
        /// <summary>
        /// Valor do status de severidade, baseado na string Severity vinda da base de dados.
        /// </summary>
        public AlarmSeverity AlarmSeverity
        {
            get
            {
                AlarmSeverity aux;
                switch (SeverityStatus)
                {
                    case "CRITICO":
                        aux = AlarmSeverity.CRITICAL;
                        break;
                    case "ALERTA":
                        aux = AlarmSeverity.ALERT;
                        break;
                    default:
                        aux = AlarmSeverity.NORMAL;
                        break;
                }

                return aux;
            }
        }
        /// <summary>
        /// Cria um objeto do Tipo Equipament com as opções passadas
        /// </summary>
        public Equipment()
        {

        }
        /// <summary>
        /// Cria um objeto do Tipo Equipment com as opções passadas
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static Equipment Load(string name, string description)
        {
            Equipment equipment = new Equipment();
            equipment.Name = name;
            equipment.Description = description;

            return equipment;
        }
        /// <summary>
        /// Cria um objeto do Tipo Equipment com as opções passadas
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="variables"></param>
        /// <returns></returns>
        public static Equipment Load(string name, string description, List<Variable> variables)
        {
            Equipment equipment = new Equipment();
            equipment.Name = name;
            equipment.Description = description;
            equipment.Variables = variables;

            return equipment;
        }
        /// <summary>
        /// Cria um objeto do Tipo Equipment com as opções passadas
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="severityStatus"></param>
        /// <param name="timeStampSeverityStatus"></param>
        /// <returns></returns>
        public static Equipment Load(string name, string description, string severityStatus, DateTime timeStampSeverityStatus) {
            Equipment equipment = new Equipment();
            
            equipment.Name = name;
            equipment.Description = description;
            equipment.SeverityStatus = severityStatus;
            equipment.TimeStampSeverityStatus = timeStampSeverityStatus;

            return equipment;
        }
    }
}
