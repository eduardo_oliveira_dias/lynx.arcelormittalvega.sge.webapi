﻿using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.Indicators
{
    /// <summary>
    /// Classe que estrutura dados de indicadores e kpis.
    /// </summary>
    public class Indicator
    {
        /// <summary>
        /// Nome do Indicador.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Valor do Indicador
        /// </summary>
        public object Value { get; set; }
        /// <summary>
        /// Timestamp do Indicador.
        /// </summary>
        public DateTime TimeStamp { get; set; }
        /// <summary>
        /// Nível do elemento referência desse indicador.
        /// </summary>
        public Level Level { get; set; }
        /// <summary>
        /// Nome(CÓDIGO SAP) do elemento referênca desse indicador.
        /// </summary>
        public string LevelName { get; set; }
        /// <summary>
        /// Descrição do elemeto referência desse indicador.
        /// </summary>
        public string LevelDescription { get; set; }
        /// <summary>
        /// Cria um objeto do Tipo Indicator com as opções passadas
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        public static Indicator Load(string name, object value, DateTime timestamp)
        {
            Indicator indicator = new Indicator();
            indicator.Name = name;
            indicator.Value = value;
            indicator.TimeStamp = timestamp;

            return indicator;
        }
        /// <summary>
        /// Cria um objeto do Tipo Indicator com as opções passadas
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="timestamp"></param>
        /// <param name="level"></param>
        /// <param name="levelName"></param>
        /// <param name="levelDescription"></param>
        /// <returns></returns>
        public static Indicator Load(string name, object value, DateTime timestamp, Level level, string levelName, string levelDescription)
        {
            Indicator indicator = new Indicator();
            indicator.Name = name;
            indicator.Value = value;
            indicator.TimeStamp = timestamp;
            indicator.Level = level;
            indicator.LevelName = levelName;
            indicator.LevelDescription = levelDescription;

            return indicator;
        }
    }
}
