﻿using Lynx.ArcelorMittalVega.SGE.Domain.Alarms.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.Variables
{
    /// <summary>
    /// Classe responsável por estruturar os dados de variável do equipamento.
    /// </summary>
    public class Variable
    {
        /// <summary>
        /// Nome da variável (CÓDIGO SAP)
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Descrição da variável.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// IP Plant Area da variável.
        /// </summary>
        public string IPPlantArea { get; set; }
        /// <summary>
        /// Unidade de engenharia do valor
        /// </summary>
        public string EngUnits { get; set; }
        /// <summary>
        /// Valor da variável.
        /// </summary>
        public object Value { get; set; }
        /// <summary>
        /// Qualidade do valor encontrado.
        /// </summary>
        public object Quality { get; set; }
        /// <summary>
        /// Valor high high definido na variável.
        /// </summary>
        public object IP_High_High_Limit { get; set; }
        /// <summary>
        /// Valor high definido na variável.
        /// </summary>
        public object IP_High_Limit { get; set; }
        /// <summary>
        /// Valor low low definido na variável.
        /// </summary>
        public object IP_Low_Low_Limit { get; set; }
        /// <summary>
        /// Valor low definido na variável.
        /// </summary>
        public object IP_Low_Limit { get; set; }
        /// <summary>
        /// Valor graph max definido na variável.
        /// </summary>
        public object IP_Graph_Maximum { get; set; }
        /// <summary>
        /// Valor graph min definido na variável.
        /// </summary>
        public object IP_Graph_Minimum { get; set; }
        /// <summary>
        /// Nome do equipamento (CÓDIGO SAP) que a variável está inserida.
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// Descrição do equipamento que a variável está inserida.
        /// </summary>
        public string EquipmentDescription { get; set; }
        /// <summary>
        /// Status do alarme em formato string, vinda da base de dados.
        /// </summary>
        public string Severity { get; set; }
        /// <summary>
        /// Valor do status de severidade, baseado na string Severity vinda da base de dados.
        /// </summary>
        public AlarmSeverity AlarmSeverity
        {
            get
            {
                AlarmSeverity aux;
                switch (Severity)
                {
                    case "CRITICO":
                        aux = AlarmSeverity.CRITICAL;
                        break;
                    case "ALERTA":
                        aux = AlarmSeverity.ALERT;
                        break;
                    default:
                        aux = AlarmSeverity.NORMAL;
                        break;
                }

                return aux;
            }
        }

        public Variable()
        {

        }
        /// <summary>
        /// Cria um objeto do Tipo Variable com as opções passadas
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="value"></param>
        /// <param name="equipmentName"></param>
        /// <param name="equipmentDescription"></param>
        /// <param name="ipPlantArea"></param>
        /// <param name="engUnits"></param>
        /// <param name="quality"></param>
        /// <param name="highHigh"></param>
        /// <param name="high"></param>
        /// <param name="lowLow"></param>
        /// <param name="low"></param>
        /// <param name="severity"></param>
        /// <returns></returns>
        public static Variable Load(string name, string description, object value, string equipmentName, string equipmentDescription, string ipPlantArea, string engUnits, object quality, object highHigh, object high, object lowLow, object low, string severity)
        {
            Variable variable = new Variable();
            variable.Name = name;
            variable.Description = description;
            variable.Value = value;
            variable.EquipmentName = equipmentName;
            variable.EquipmentDescription = equipmentDescription;
            variable.IPPlantArea = ipPlantArea;
            variable.EngUnits = engUnits;
            variable.Quality = quality;
            variable.IP_High_High_Limit = highHigh;
            variable.IP_High_Limit = high;
            variable.IP_Low_Low_Limit = lowLow;
            variable.IP_Low_Limit = low;
            variable.Severity = severity;

            return variable;
        }

        /// <summary>
        /// Cria um objeto do Tipo Variable com as opções passadas
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="value"></param>
        /// <param name="equipmentName"></param>
        /// <param name="equipmentDescription"></param>
        /// <param name="ipPlantArea"></param>
        /// <param name="engUnits"></param>
        /// <param name="quality"></param>
        /// <param name="highHigh"></param>
        /// <param name="high"></param>
        /// <param name="lowLow"></param>
        /// <param name="low"></param>
        /// <param name="graphMax"></param>
        /// <param name="graphMin"></param>
        /// <param name="severity"></param>
        /// <returns></returns>
        public static Variable Load(string name, string description, object value, string equipmentName, string equipmentDescription, string ipPlantArea, string engUnits, object quality, object highHigh, object high, object lowLow, object low, object graphMax, object graphMin, string severity)
        {
            Variable variable = new Variable();
            variable.Name = name;
            variable.Description = description;
            variable.Value = value;
            variable.EquipmentName = equipmentName;
            variable.EquipmentDescription = equipmentDescription;
            variable.IPPlantArea = ipPlantArea;
            variable.EngUnits = engUnits;
            variable.Quality = quality;
            variable.IP_High_High_Limit = highHigh;
            variable.IP_High_Limit = high;
            variable.IP_Low_Low_Limit = lowLow;
            variable.IP_Low_Limit = low;
            variable.IP_Graph_Maximum = graphMax;
            variable.IP_Graph_Minimum = graphMin;
            variable.Severity = severity;

            return variable;
        }
    }

}
