﻿using Lynx.ArcelorMittalVega.SGE.Domain.Equipments;
using Lynx.ArcelorMittalVega.SGE.Domain.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.Reports
{
    /// <summary>
    /// Classe que estrutura o relatório de equipamentos
    /// </summary>
    public class ReportEquipments
    {
        /// <summary>
        /// Dados do gráfico de barras com os alarmes por área.
        /// </summary>
        public List<GraphicBar> GraphicEquipmentStatus { get; set; }
        /// <summary>
        /// Dados do gráfico de tendência dos KPI's das áreas
        /// </summary>
        public List<GraphicTrend> GraphicKPIs { get; set; }
        /// <summary>
        /// Lista de alarmes dos equipamentos encontrados.
        /// </summary>
        public List<Equipment> EquipmentsStatus{ get; set; }
    }
}
