﻿using System;

namespace Lynx.ArcelorMittalVega.SGE.Domain
{
    public class DomainException : Exception
    {
        public DomainException(string message) : base(message) { }
    }
}
