﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.Users
{
    /// <summary>
    /// Classe que estrutura os dados dos usuários da rede.
    /// </summary>
    public class UserAD
    {
        /// <summary>
        /// Nome de apresentação, por extenso.
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// Nome de usuario, usado para logar no sistema.
        /// </summary>
        public string SamAccountName { get; set; }
        /// <summary>
        /// Nome de usuario, usado para logar no sistema.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Senha, utilizada apenas no login.
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Lista de funções que o usuário possui dentro do sistema. Pode ser usuário comum ou admin.
        /// </summary>
        public string[] Roles { get; set; }
        /// <summary>
        /// Carrega o usuário conforme o usuário encontrado na rede.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static UserAD FromUserPrincipal(UserPrincipal user)
        {
            return new UserAD
            {
                DisplayName = user.DisplayName,
                SamAccountName = user.SamAccountName,
                Name = user.Name,
            };
        }
        
    }
}
