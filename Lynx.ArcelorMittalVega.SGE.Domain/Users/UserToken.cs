﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.Users
{
    /// <summary>
    /// Token de usuário, necessário para autorizar o acesso à API.
    /// </summary>
    public class UserToken
    {
        /// <summary>
        /// Flag que indica se está autenticado ou não.
        /// </summary>
        public bool Authenticated { get; set; }
        /// <summary>
        /// Chave Jwt para o acesso à api.
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// Data de expiração desse token.
        /// </summary>
        public DateTime Expiration { get; set; }
        /// <summary>
        /// Mensagem de retorno se o token foi gerado com sucesso.
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Usuário que gerou o token
        /// </summary>
        public UserAD UserLogged { get; set; }
    }
}
