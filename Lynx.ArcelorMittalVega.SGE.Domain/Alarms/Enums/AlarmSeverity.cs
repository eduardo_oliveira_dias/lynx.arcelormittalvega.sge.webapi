﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.Alarms.Enums
{
    /// <summary>
    /// Enum de status dos alarmes.
    /// </summary>
    public enum AlarmSeverity
    {
        CRITICAL,
        ALERT,
        NORMAL
    }
}
