﻿using Lynx.ArcelorMittalVega.SGE.Domain.Alarms.Enums;
using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.Alarms
{
    /// <summary>
    /// Classe que identifica um alarme de variável.
    /// </summary>
    public class Alarm
    {
        /// <summary>
        /// Nome da variável.
        /// </summary>
        public string VariableName { get; set; }
        /// <summary>
        /// Descrição da variável
        /// </summary>
        public string VariableDescription { get; set; }
        /// <summary>
        /// Unidade de engenharia
        /// </summary>
        public string EngUnit { get; set; }
        /// <summary>
        /// Valor da variável
        /// </summary>
        public object Value { get; set; }
        /// <summary>
        /// Timestamp do valor encontrado
        /// </summary>
        public DateTime TimeStamp { get; set; }
        /// <summary>
        /// Nome (Código SAP) do equipamento onde a variável está
        /// </summary>
        public string EquipmentName { get; set; }
        /// <summary>
        /// Descrição do equipamento onde a variável está
        /// </summary>
        public string EquipmentDescription{ get; set; }
        /// <summary>
        /// Nome (Código SAP) da área do equipamento onde a variável está
        /// </summary>
        public string AreaName{ get; set; }
        /// <summary>
        /// Descrição da área do equipamento onde a variável está
        /// </summary>
        public string AreaDescription { get; set; }
        /// <summary>
        /// Nome (Código SAP) da seção do equipamento onde a variável está
        /// </summary>
        public string SectionName{ get; set; }
        /// <summary>
        /// Descrição da seção do equipamento onde a variável está
        /// </summary>
        public string SectionDescription { get; set; }
        /// <summary>
        /// Nome (Código SAP) do segmento do equipamento onde a variável está
        /// </summary>
        public string SegmentName{ get; set; }
        /// <summary>
        /// Descrição do segmento do equipamento onde a variável está
        /// </summary>
        public string SegmentDescription { get; set; }
        /// <summary>
        /// Status do alarme em formato string, vinda da base de dados.
        /// </summary>
        public string Severity { get; set; }
        /// <summary>
        /// Valor do status de severidade, baseado na string Severity vinda da base de dados.
        /// </summary>
        public AlarmSeverity AlarmSeverity { get {
                AlarmSeverity aux;
                switch (Severity)
                {
                    case "CRITICO":
                        aux = AlarmSeverity.CRITICAL;
                        break;
                    case "ALERTA":
                        aux = AlarmSeverity.ALERT;
                        break;
                    default: 
                        aux = AlarmSeverity.NORMAL;
                        break;
                }

                return aux;
            }
        }

        public Alarm()
        {

        }
        /// <summary>
        /// Cria um objeto do Tipo Alarm com as opções passadas.
        /// </summary>
        /// <param name="variableName"></param>
        /// <param name="variableDescription"></param>
        /// <param name="engUnit"></param>
        /// <param name="value"></param>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        public static Alarm Load(string variableName, string variableDescription, string engUnit, object value, DateTime timeStamp)
        {
            Alarm alarm = new Alarm();
            alarm.VariableName = variableName;
            alarm.VariableDescription = variableDescription;
            alarm.EngUnit = engUnit;
            alarm.Value = value;
            alarm.TimeStamp = timeStamp;

            return alarm;
        }

        /// <summary>
        /// /// Cria um objeto do Tipo Alarm com as opções passadas.
        /// </summary>
        /// <param name="variableName"></param>
        /// <param name="variableDescription"></param>
        /// <param name="engUnit"></param>
        /// <param name="value"></param>
        /// <param name="timeStamp"></param>
        /// <param name="equipmentName"></param>
        /// <param name="equipmentDescription"></param>
        /// <param name="areaName"></param>
        /// <param name="areaDescription"></param>
        /// <param name="sectionName"></param>
        /// <param name="sectionDescription"></param>
        /// <param name="segmentName"></param>
        /// <param name="segmentDescription"></param>
        /// <param name="severity"></param>
        /// <returns></returns>
        public static Alarm Load(string variableName, string variableDescription, string engUnit, object value, DateTime timeStamp, string equipmentName, string equipmentDescription, string areaName, string areaDescription, string sectionName, string sectionDescription, string segmentName, string segmentDescription, string severity)
        {
            Alarm alarm = new Alarm();
            alarm.VariableName = variableName;
            alarm.VariableDescription = variableDescription;
            alarm.EngUnit = engUnit;
            alarm.Value = value;
            alarm.TimeStamp = timeStamp;
            alarm.EquipmentName = equipmentName;
            alarm.EquipmentDescription = equipmentDescription;
            alarm.AreaName = areaName;
            alarm.AreaDescription = areaDescription;
            alarm.SectionName = sectionName;
            alarm.SectionDescription = sectionDescription;
            alarm.SegmentName = segmentName;
            alarm.SegmentDescription = segmentDescription;
            alarm.Severity = severity;

            return alarm;
        }

    }
}
