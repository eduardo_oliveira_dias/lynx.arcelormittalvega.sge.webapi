﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain
{
    /// <summary>
    /// Classe abstrata para estruturação de filtros de pesquisa de relatório.
    /// </summary>
    public abstract class AbstractReportFilter
    {
        /// <summary>
        /// Data início da pesquisa.
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// Data fim da pesquisa.
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// Nome do Segmento da pesquisa.
        /// </summary>
        public string SegmentName { get; set; }
        /// <summary>
        /// Nome da área da pesquisa
        /// </summary>
        public string AreaName { get; set; }
        /// <summary>
        /// Nome da seção da pesquisa.
        /// </summary>
        public string SectionName { get; set; }
        /// <summary>
        /// Nome do equipamento da pesquisa da pesquisa.
        /// </summary>
        public string EquipmentName { get; set; }
    }
}