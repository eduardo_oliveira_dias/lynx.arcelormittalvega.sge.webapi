﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Domain.SystemConfigurations
{
    /// <summary>
    /// Classe que estrutura as configurações do sistema.
    /// </summary>
    public class SystemConfiguration
    {
        /// <summary>
        /// Chave, ou nome da configuração.
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Valor da configuração.
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Carrega a configuração com os dados passados.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static SystemConfiguration Load(string key, string value)
        {
            SystemConfiguration config = new SystemConfiguration();
            config.Key = key;
            config.Value = value;

            return config;
        }
    }
}
