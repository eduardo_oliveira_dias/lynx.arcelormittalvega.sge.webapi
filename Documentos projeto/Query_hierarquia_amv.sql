--= ----------------------------------------------------
--= OWNER: ARCELOR MITTAL - VEGA
--= ----------------------------------------------------
--= AUTOR: EDUARDO DIAS
--= DATA CRIACAO: 03/02/2021
--= ----------------------------------------------------
--= MODULO: SISTEMA DE GESTÃO DE EQUIPAMENTOS
--= ----------------------------------------------------
--= DESCRICAO:
--= ----------
--= Query para trazer os níveis da hierarquia SAP VIEW
--= até o nível de Equipamento (NIVEL 5), cujo tamanho do nome
--= é 16, seguindo o padrão BV-xx-xxxx-xx-xx. Para os equipamentos 
--= serão trazidos também os alarmes críticos.
--= ----------------------------------------------------
--= 
--= ----------------------------------------------------
--= REVISÕES
--= ----------------------------------------------------
--= SEM REVISÕES
--= ----------------------------------------------------


(select  CHAR_LENGTH(f.NAME) TAMANHO, 
		f.Name, 
		f.Description, 
		f.FOLDER_PARENT, 
		0 Alarmes -- 0 por padrão nos níveis que não têm variáveis
from folderDef f
where f.name like '%bv%'
	AND TAMANHO < 16
)
UNION
(select  CHAR_LENGTH(f2.NAME) TAMANHO, f2.Name, f2.Description, f2.FOLDER_PARENT,
	 count(q.name) Alarmes
 from folderDef f2
LEFT JOIN (SELECT iad.name
		FROM IP_AnalogDef iad
		where (IP_VALUE >= IP_HIGH_HIGH_LIMIT OR IP_VALUE <= IP_LOW_LOW_LIMIT) -- ALARMES CRITICOS SÃO CARACTERIZADOS QUANDO ALCANÇAM OU ULTRAPASSAM OS VALORES HIGH_HIGH E LOW_LOW
)q ON  f2.record_name = q.name
where f2.name like '%bv%' 
	AND TAMANHO = 16
GROUP BY f2.name, f2.Description, f2.FOLDER_PARENT
)

ORDER BY ALARMES desc,TAMANHO, name


