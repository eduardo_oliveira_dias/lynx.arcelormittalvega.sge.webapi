﻿using Lynx.ArcelorMittalVega.SGE.Domain.Alarms;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Results
{
    /// <summary>
    /// Classe <c>GraphicResult</c>
    /// <para>Classe responsável por estruturar os dados em formato de gráfico e os alarmes que geraram o gráfico.</para>
    /// </summary>
    /// <typeparam name="T">Classe de dados estruturados para gráfico.</typeparam>
    public class GraphicResult<T> where T : class
    {
        /// <summary>
        /// Dados estruturados informando o valor e o nome da informação a serem plotada no gráfico.
        /// </summary>
        public T GraphicData { get; set; }
        /// <summary>
        /// Lista de alarmes que geraram os dados do gráfico.
        /// </summary>
        public List<Alarm> Alarms { get; set; } = new List<Alarm>();

    }
}
