﻿using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Results
{
    /// <summary>
    /// Classe <c>PlantLevelResult</c>
    /// <para>Classe que estrutura os dados dos níveis da planta.</para>
    /// </summary>
    public sealed class PlantLevelResult
    {
        /// <summary>
        /// Nome do elemento (Código SAP).
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Descrição do elemento, que é um nome amigável para visualização no sistema.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Nível na hierarquia (Planta, Segmento, Área, Seção ou Equipamento).
        /// </summary>
        public string Level { get; set; }
        /// <summary>
        /// Elemento pai desse elemento.
        /// </summary>
        public PlantLevelResult Parent { get; set; }
        /// <summary>
        /// Elementos do mesmo tipo que são filhos desse elemento.
        /// </summary>
        public List<PlantLevelResult> Children { get; set; }
        /// <summary>
        /// Contagem de alarmes ativos para esse elemento.
        /// </summary>
        public int AlarmCount { get; set; }
        /// <summary>
        /// Nome do pai do elemento.
        /// </summary>
        public string ParentName { get; set; }

        public PlantLevelResult()
        {

        }

        public PlantLevelResult(PlantLevel plantLevel)
        {
            Name = plantLevel.Name;
            Description = plantLevel.Description;
            Level = plantLevel.Level;
            AlarmCount = plantLevel.AlarmCount;
            ParentName = plantLevel.ParentName;
        }
    }
}
