﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Util
{
    /// <summary>
    /// Classe <c>MessageUser</c>
    /// <para>Classe responsável pelas mensagens de retorno para o usuário.</para>
    /// </summary>
    public class MessageUser
    {
        /// <summary>
        /// Mensagem de exceção inesperada, quando ocorre algum erro não previsto pelo sistema.
        /// </summary>
        /// <returns>Mensagem de erro "Exceção inesperada"</returns>
        public static string UnexpectedException()
        {
            return "Exceção Inesperada";
        }
        /// <summary>
        /// Mensagem de erro quando o valor de Data início é maior que Data Fim.
        /// </summary>
        /// <returns>Mensagem de erro: "Data Fim não pode ser inferior à Data Início."</returns>
        public static string StartTimeGreaterThanEndTime()
        {
            return "Data Fim não pode ser inferior à Data Início.";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Mensagem de erro: "Data Fim não pode ser superior que a data atual."</returns>
        public static string EndTimeGreaterThanCurrentDate()
        {
            return "Data Fim não pode ser superior que a data atual.";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectName">Nome do objeto que foi criado.</param>
        /// <returns>Mensagem de Sucesso: "<nome_do_objeto> criado(a) com sucesso!"</returns>
        public static string CreateSucceed(string objectName)
        {
            return $"{objectName} criado(a) com sucesso!";
        }
        /// <summary>
        /// Mensagem de erro informando que determinado campo é obrigatório
        /// </summary>
        /// <param name="fieldName">Nome do campo.</param>
        /// <returns>Mensagem de erro: "<nome_do_campo> é obrigatório(a)"</returns>
        public static string RequiredField(string fieldName)
        {
            return $"{fieldName} é obrigatório(a)";
        }
        /// <summary>
        /// Mensagem de erro informando que o objeto está com formato inválido
        /// </summary>
        /// <param name="objectName">Nome do Objeto.</param>
        /// <returns>Mensagem de erro: "<nome_do_objeto> possui formato inválido(a)."</returns>
        public static string InvalidFormat(string objectName)
        {
            return $"{objectName} possui formato inválido(a).";
        }

        /// <summary>
        /// Mensagem de sucesso informando que o objeto foi salvo.
        /// </summary>
        /// <param name="objectName">Nome do Objeto.</param>
        /// <returns>Mensagem de sucesso: "<nome_do_objeto> salvo(a) com sucesso!"</returns>
        public static string SaveSucceed(string objectName)
        {
            return $"{objectName} salvo(a) com sucesso!";
        }

        /// <summary>
        /// Mensagem de sucesso informando que um ou mais objetos foram salvos.
        /// </summary>
        /// <param name="objectName">Nome do Objeto.</param>
        /// <returns>Mensagem de sucesso: "<nomes_dos_objetos> salvo(a)s com sucesso!"</returns>
        public static string SaveSucceedMany(string objectName)
        {
            return $"{objectName} salvo(a)s com sucesso!";
        }
        /// <summary>
        /// Mensagem de sucesso informando que um ou mais objetos foram excluídos.
        /// </summary>
        /// <param name="objectName">Nome do Objeto.</param>
        /// <returns>Mensagem de sucesso: "<nomes_dos_objetos> excluídos(a)s com sucesso!"</returns>
        public static string DeleteSucceed(string objectName)
        {
            return $"{objectName} excluído(a)s com sucesso!";
        }

        /// <summary>
        /// Mensagem de erro informando que o tipo de arquivo enviado é de formato inválido.
        /// </summary>
        /// <returns>Mensagem de erro: "Tipo de arquivo inválido."</returns>
        public static string InvalidFileFormat()
        {
            return "Tipo de arquivo inválido.";
        }
        /// <summary>
        /// Mensagem de erro informando que a tag não foi encontrada na base de dados.
        /// </summary>
        /// <returns>Mensagem de erro: "Tag não encontrada."</returns>
        public static string VariableNotFound()
        {
            return "Tag não encontrada.";
        }
        /// <summary>
        /// Mensagem de erro informando que não foi possível salvar o objeto.
        /// </summary>
        /// <param name="objectName">Nome do objeto</param>
        /// <returns>Mensagem de erro: "Erro ao salvar valor de <nome_objeto>."</returns>
        public static string SaveFailed(string objectName)
        {
            return $"Erro ao salvar valor de {objectName}.";
        }
        /// <summary>
        /// Mensagem de erro informando que determinado arquivo não foi encontrado.
        /// </summary>
        /// <returns>Mensagem de erro: "Arquivo não encontrado."</returns>
        public static string FileNotFound()
        {
            return $"Arquivo não encontrado.";
        }
        /// <summary>
        /// Mensagem de erro informando que o usuário ou senha estão incorretos.
        /// </summary>
        /// <returns>Mensagem de erro: "Usuario e/ou senha inválidos."</returns>
        public static string InvalidUserNameOrPassword()
        {
            return "Usuario e/ou senha inválidos.";
        }
        /// <summary>
        /// Mensagem de erro informando que o usuário não possui permissão de acessar o sistema.
        /// </summary>
        /// <param name="userName">Nome do usuário.</param>
        /// <returns>Mensagem de erro: "Usuário <nome_usuario> não possui permissão de acessar o Sistema."</returns>
        public static string UnauthorizedUser(string userName)
        {
            return $"Usuário {userName} não possui permissão de acessar o Sistema.";
        }
    }
}
