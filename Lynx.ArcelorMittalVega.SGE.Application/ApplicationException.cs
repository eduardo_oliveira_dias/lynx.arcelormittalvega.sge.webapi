﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application
{
    public class ApplicationException : Exception
    {
        public ApplicationException(string businessMessage) : base(businessMessage) { }
    }
}
