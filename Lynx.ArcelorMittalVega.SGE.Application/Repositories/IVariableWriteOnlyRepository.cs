﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Repositories
{
    public interface IVariableWriteOnlyRepository
    {
        /// <summary>
        /// Método que atualiza o valor do parâmetro da variável.
        /// </summary>
        /// <param name="variableName">Nome da variável.</param>
        /// <param name="parameter">Nome do parâmetro.</param>
        /// <param name="value">Valor do parâmetro.</param>
        void Update(string variableName, string parameter, object value);
    }
}
