﻿using Lynx.ArcelorMittalVega.SGE.Domain.SystemConfigurations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Repositories
{
    public interface IConfigurationReadOnlyRepository
    {
        /// <summary>
        /// Retorna o valor de determinada configuração.
        /// </summary>
        /// <param name="key">Chave da configuração para busca.</param>
        /// <returns>Valor da variável.</returns>
        public object Get(string key);
        /// <summary>
        /// Método que consulta as configurações gerais da aplicação.
        /// </summary>
        /// <returns>Lista de objetos chave-valor com as configuraões cadastradas.</returns>
        public List<SystemConfiguration> GetGeneralConfigurations();
        /// <summary>
        /// Método que retorna a lista dos parametros de variável a serem apresentados na tela de equipamentos
        /// </summary>
        /// <returns>Lista de objetos chave-valor com os nomes dos parâmetros das variáveis</returns>
        public List<SystemConfiguration> GetVariableParameters();
        /// <summary>
        /// Método responsável por consultar o posicionamento dos gráficos dos elementos de cada nível
        /// </summary>
        /// <returns>Lista de objetos chave-valor com o posicionamento x e y de cada elemento.</returns>
        public List<SystemConfiguration> GetObjectPositions();
    }
}
