﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.Application.Repositories
{
    public interface IImageWriteOnlyRepository
    {
        /// <summary>
        /// Método Responsável de salvar a nova imagem no servidor.
        /// </summary>
        /// <param name="file">O arquivo de imagem a ser salva.</param>
        /// <param name="path">O caminho a ser salvo.</param>
        /// <param name="filename">O nome do arquivo.</param>
        /// <returns>O nome do arquivo salvo.</returns>
        string Upload(IFormFile file, string path, string filename);
    }
}
