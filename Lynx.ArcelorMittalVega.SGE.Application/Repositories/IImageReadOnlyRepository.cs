﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.Application.Repositories
{
    public interface IImageReadOnlyRepository
    {
        /// <summary>
        /// Método responsável por consultar o caminho da imagem por nome do equipamento.
        /// </summary>
        /// <param name="name">Nome do equipamento</param>
        /// <returns>O caminho da imagem.</returns>
        string GetPath(string name);
    }
}
