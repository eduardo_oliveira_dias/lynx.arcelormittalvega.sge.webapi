﻿using Lynx.ArcelorMittalVega.SGE.Domain.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Repositories
{
    public interface IUserReadOnlyRepository
    {
        /// <summary>
        /// Método responsável por validar o usuário e senha da instância de usuário 
        /// </summary>
        /// <param name="user">Usuário a ser validado, serão utilizados os campos <value>Name</value> e <value>Password</value> da classe: <c>UserAD</c> </param>
        /// <returns>True se o login for bem sucedido, False se o usuário ou senha estiverem incorretos</returns>
        bool Login(UserAD user);
        /// <summary>
        /// Método responsável por consultar quais as "Roles" do Ip21 que o usuário é membro.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Um array(<c>string[]</c>) com os nomes das Roles do Usuário.</returns>
        public string[] GetUserRoles(UserAD user);
        [Obsolete]
        public UserAD GetUserInfo(UserAD user);
    }
}
