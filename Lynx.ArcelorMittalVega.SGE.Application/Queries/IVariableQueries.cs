﻿using Lynx.ArcelorMittalVega.SGE.Domain.Variables;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Queries
{
    public interface IVariableQueries
    {
        /// <summary>
        /// Método que consulta a variável no IP21 por nome ou descrição.
        /// </summary>
        /// <param name="name">Nome a ser buscado.</param>
        /// <returns>Lista de variáveis que correspondem à busca.</returns>
        List<Variable> GetVariables(string name);
        /// <summary>
        /// Método que consulta as variáveis do equipamento passado.
        /// </summary>
        /// <param name="equipmentName">nome do equipamento.</param>
        /// <returns>Lista de variáveis do equipamento.</returns>
        List<Variable> GetEquipmentVariables(string equipmentName);
    }
}
