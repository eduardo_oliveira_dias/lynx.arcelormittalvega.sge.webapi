﻿using Lynx.ArcelorMittalVega.SGE.Application.Results;
using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.Application.Queries
{
    public interface IPlantLevelQueries
    {
        /// <summary>
        /// Método responsável por consultar os níveis(folders) e seus respectivos pais, para montar a árvore.
        /// </summary>
        /// <returns>O objeto com a árvore montada.</returns>
        PlantLevel GetPlantLevels();
        /// <summary>
        /// Método que pequisa os níveis no IP21 a partir do nome ou  descrição.
        /// </summary>
        /// <param name="query">Nome ou descrição a ser buscada.</param>
        /// <returns>Lista dos níveis encontrados</returns>
        List<PlantLevel> GetPlantLevels(string query);
        /// <summary>
        /// Método responsável por consultar determinados níveis da hierarquia a partir do nome e do nome do nível pai.
        /// </summary>
        /// <param name="levelName">Nome do nível desejado</param>
        /// <param name="parentName">Nome do nível pai</param>
        /// <returns>Lista dos níveis encontrados</returns>
        List<PlantLevel> GetPlantLevelsByLevelName(string levelName, string parentName);
    }
}
