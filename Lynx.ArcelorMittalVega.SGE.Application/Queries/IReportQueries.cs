﻿using Lynx.ArcelorMittalVega.SGE.Domain;
using Lynx.ArcelorMittalVega.SGE.Domain.Equipments;
using Lynx.ArcelorMittalVega.SGE.Domain.Graphics;
using Lynx.ArcelorMittalVega.SGE.Domain.Reports;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Queries
{
    public interface IReportQueries
    {

        /// <summary>
        /// Método que consulta os alarmes no período e estrutura os dados em gráfico de barra.
        /// </summary>
        /// <param name="filter">Objeto de filtro com a localização do equipamento, e a data desejada.</param>
        /// <returns>Lista de alarmes agrupados por área para apresentação no gráfico de barra.</returns>
        public List<GraphicBar> GetGraphicBar(AbstractReportFilter filter);
        /// <summary>
        /// Método que consulta os KPIs das áreas, dentro do período, estruturando os dados para apresentação em gráfico de linha.
        /// </summary>
        /// <param name="filter">Objeto de filtro com a localização da data, e a data desejada.</param>
        /// <returns>Lista de kpis agrupados por área e timestamp para apresentação no gráfico de linha.</returns>
        public List<GraphicTrend> GetGraphicTrendKPI(AbstractReportFilter filter);
        /// <summary>
        /// Método responsável por filtrar os últimos alarmes status de cada equipamento, priorizando a severidade Crítica e após isso Alerta.
        /// </summary>
        /// <param name="filter">Objeto de filtro com a localização do equipamento, e a data desejada.</param>
        /// <returns>Lista de equipamentos com alarme no período.</returns>
        public List<Equipment> GetEquipmentStatus(AbstractReportFilter filter);
        /// <summary>
        /// Método que trás os dados agrupados em formato de relatório, para consulta única.
        /// </summary>
        /// <param name="filter">Objeto de filtro com a localização da data, e a data desejada.</param>
        /// <returns>O Objeto de relatório com a lista de equipamentos e os gráficos.</returns>
        public ReportEquipments GetReportEquipments(AbstractReportFilter filter);
    }
}
