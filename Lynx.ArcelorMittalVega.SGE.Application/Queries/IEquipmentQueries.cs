﻿using Lynx.ArcelorMittalVega.SGE.Domain.Equipments;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Queries
{
    public interface IEquipmentQueries
    {
        /// <summary>
        /// Consulta o equipamento na árvore filtrando por nome.
        /// </summary>
        /// <param name="name">Nome do Equipamento.</param>
        /// <returns><c>Equipment</c> Equipamento encontrado.</returns>
        Equipment GetEquipment(string name);
        /// <summary>
        /// Consulta todos os equipamentos na árvore SAP VIEW
        /// </summary>
        /// <returns><c>List<Equipment></c>Lista de Equipamentos encontrados.</returns>
        List<Equipment> GetAll();
    }
}
