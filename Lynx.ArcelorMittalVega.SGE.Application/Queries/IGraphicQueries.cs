﻿using Lynx.ArcelorMittalVega.SGE.Application.Results;
using Lynx.ArcelorMittalVega.SGE.Domain.Alarms;
using Lynx.ArcelorMittalVega.SGE.Domain.Graphics;
using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Queries
{
    public interface IGraphicQueries
    {
        /// <summary>
        /// Método que consulta os alarmes ativos e estrutura os dados para montar os gráficos de setor(pizza) para os níveis da hierarquia.
        /// </summary>
        /// <param name="childrenLevel">O nível filho do nível que está sendo mostrado, para montagem dos gráficos agrupando por nível. 
        /// Ex.: O nível Segmento tem como nível filho as áreas.</param>
        /// <param name="parentLevel">Nível que está sendo mostrado.</param>
        /// <param name="parentLevelName">Nome do nível que está sendo mostrado.</param>
        /// <returns>Lista de gráfico de setor para cada filho do nível.</returns>
        public IEnumerable<GraphicSector> GetAlarmsGraphicSector(string childrenLevel, string parentLevel, string parentLevelName);
        /// <summary>
        /// Método responsável por consultar os dados históricos de uma lista de variáveis e estruturar os dados para o gráfico de tendência
        /// (linha).
        /// </summary>
        /// <param name="variableNames">A lista de nomes das variáveis</param>
        /// <param name="startTime">Data início da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <returns>A lista de dados de gráfico de tendência.</returns>
        public IEnumerable<GraphicTrend> GetAlarmsGraphicTrend(List<string> variableNames, DateTime startTime, DateTime endTime);
        /// <summary>
        /// Método para trazer os alarmes por tipo em formato de gráfico de setor(pizza), dentro do período passado.
        /// </summary>
        /// <param name="startTime">Data início da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <param name="level">Nível da hierarquia para agrupar os alarmes.</param>
        /// <param name="levelName">Nome do nível para filtrar os alarmes.</param>
        /// <returns>Um objeto com os dados estruturados em gráfico de setor(pizza), junto a lista de alarmes encontrados.</returns>
        public GraphicResult<GraphicSector> GetAlarmsGraphicType(DateTime startTime, DateTime endTime, Level level, string levelName);
        /// <summary>
        /// Método responsável pela pesquisa de alarmes agrupando por nível e depois por tipo, para cada nível da hierarquia. Os dados serão
        /// estruturados em formato de barra.
        /// </summary>
        /// <param name="startTime">Data início da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <param name="level">Nível da hierarquia para agrupar os alarmes.</param>
        /// <param name="levelName">Nome do nível para filtrar os alarmes.</param>
        /// <returns>Um objeto com os dados estruturados em gráfico de barra, junto a lista de alarmes encontrados.</returns>
        public GraphicResult<List<GraphicBar>> GetAlarmsGraphicBar(DateTime startTime, DateTime endTime, Level level, string levelName);
        /// <summary>
        /// Método responsável por montar o gráfico de barras apartir de uma lista de alarmes agrupando por nível e depois por tipo, 
        /// para cada nível da hierarquia.
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem tratados.</param>
        /// <param name="level">Nível da hierarquia para agrupar os alarmes.</param>
        /// <param name="levelName">Nome do nível para filtrar os alarmes.</param>
        /// <returns>Um objeto com os dados estruturados em gráfico de barra, junto a lista de alarmes encontrados.</returns>
        public GraphicResult<List<GraphicBar>> MountGraphicBar(List<Alarm> alarms, Level level, string levelName);
        /// <summary>
        /// Método responsável por montar o gráfico de setor(pizza) a partir de uma lista de alarmes agrupando por tipo para cada nível da hierarquia.
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem tratados.</param>
        /// <param name="level">Nível da hierarquia para agrupar os alarmes.</param>
        /// <param name="levelName">Nome do nível para filtrar os alarmes.</param>
        /// <returns>Um objeto com os dados estruturados em gráfico de setor(pizza), junto a lista de alarmes encontrados.</returns>
        public GraphicResult<GraphicSector> MountGraphicType(List<Alarm> alarms, Level level, string levelName);
    }
}
