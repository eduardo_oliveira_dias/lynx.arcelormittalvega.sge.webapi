﻿using Lynx.ArcelorMittalVega.SGE.Domain.Graphics;
using Lynx.ArcelorMittalVega.SGE.Domain.Indicators;
using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Queries
{
    public interface IIndicatorQueries
    {
        /// <summary>
        /// Método que consulta o valor do KPI atual do determinado ativo.
        /// </summary>
        /// <param name="itemName">Nome do ativo(Equipamento, Seção, Área ou Segmento)</param>
        /// <returns>O objeto com os dados de KPI, incluindo valor e timestamp.</returns>
        public Indicator GetKPI(string itemName);
        /// <summary>
        /// Método que consulta um ou mais valores de KPI dos ativos do nível selecionado dentro do período.
        /// </summary>
        /// <param name="startTime">Data início da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <param name="level">Nível da hierarquia para agrupar os kpi's.</param>
        /// <param name="levelName">Nome do nível para filtrar os kpi's.</param>
        /// <returns>A lista de objetos com os dados de KPI, incluindo valor e timestamp.</returns>
        public List<Indicator> GetKPI(DateTime startTime, DateTime endTime, Level level, string levelName);
        /// <summary>
        /// Método que consulta um ou mais valores de KPI dos ativos do nível selecionado dentro do período, estruturando
        /// para apresentação no gráfico de tendência.
        /// </summary>
        /// <param name="startTime">Data início da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <param name="level">Nível da hierarquia para agrupar os kpi's.</param>
        /// <param name="levelName">Nome do nível para filtrar os kpi's.</param>
        /// <returns>A lista de objetos de gráfico de tendência com os dados de KPIs.</returns>
        public List<GraphicTrend> GetGraphicTrendKPI(DateTime startTime, DateTime endTime, Level level, string levelName);
    }
}
