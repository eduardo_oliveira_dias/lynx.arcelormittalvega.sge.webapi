﻿using Lynx.ArcelorMittalVega.SGE.Domain.Alarms;
using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Queries
{
    public interface IAlarmQueries
    {
        /// <summary>
        /// Método que consulta a base de dados do IP21 na área fixa e retorna as tags com alarme ativo
        /// </summary>
        /// <returns>Uma lista de alarmes encontrados.</returns>
        List<Alarm> GetAlarms();
        /// <summary>
        /// Método que consulta a base de dados do IP21 na área fixa e retorna as tags com alarme crítico
        /// </summary>
        /// <returns>A lista de alarmes encontrados.</returns>
        List<Alarm> GetCriticalAlarms();
        /// <summary>
        ///  Método que consulta a base de dados do IP21 na área fixa e retorna as tags com alarme ativo e filtra pelo nível da hierarquia
        /// </summary>
        /// <param name="level">O nível da hierarquia, variando entre segmentName, areaName, sectionName e equipmentName.</param>
        /// <param name="name">O nome do nível selecionado.</param>
        /// <returns>>Uma lista de alarmes encontrados.</returns>
        List<Alarm> GetAlarms(string level, string name);
        /// <summary>
        /// Método que pesquisa os valores das tags de alarm dentro do período passado.
        /// </summary>
        /// <param name="startTime">Data ínicio da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <returns>A lista de alarmes encontrados e seus respectivos timestamps.</returns>
        List<Alarm> GetAlarmsTrend(DateTime startTime, DateTime endTime, string level, string levelName);

    }
}
