﻿using Lynx.ArcelorMittalVega.SGE.Application.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Commands.GetConfiguration
{
    /// <summary>
    /// Classe <c>GetConfigurationUseCase</c> 
    /// Classe de caso de uso responsável por recuperar dados de configuração do repositório.
    /// </summary>
    public class GetConfigurationUseCase : IGetConfigurationUseCase
    {
        private readonly IConfigurationReadOnlyRepository _configurationReadOnlyRepository;
        public GetConfigurationUseCase(IConfigurationReadOnlyRepository configurationReadOnlyRepository)
        {
            _configurationReadOnlyRepository = configurationReadOnlyRepository;
        }
        /// <summary>
        /// Retorna o valor de determinada configuração
        /// </summary>
        /// <param name="key">Chave da configuração para busca.</param>
        /// <returns>Valor da variável.</returns>
        public object Get(string key)
        {
            return _configurationReadOnlyRepository.Get(key);
        }
    }
}
