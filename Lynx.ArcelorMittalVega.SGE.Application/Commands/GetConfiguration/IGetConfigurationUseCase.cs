﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Commands.GetConfiguration
{
    public interface IGetConfigurationUseCase
    {
        /// <summary>
        /// Retorna o valor de determinada configuração
        /// </summary>
        /// <param name="key">Chave da configuração para busca.</param>
        /// <returns>Valor da variável.</returns>
        public object Get(string key);
    }
}
