﻿using Lynx.ArcelorMittalVega.SGE.Application.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.Application.Commands.UploadImages
{
    public class UploadImageUseCase : IUploadImageUseCase
    {
        private readonly IImageWriteOnlyRepository _imageWriteOnlyRepository;

        public UploadImageUseCase(IImageWriteOnlyRepository imageWriteOnlyRepository)
        {
            _imageWriteOnlyRepository = imageWriteOnlyRepository;
        }

        /// <summary>
        /// Método Responsável de salvar a nova imagem no servidor.
        /// </summary>
        /// <param name="file">O arquivo de imagem a ser salva.</param>
        /// <param name="path">O caminho a ser salvo.</param>
        /// <param name="filename">O nome do arquivo.</param>
        /// <returns>O nome do arquivo salvo.</returns>
        public string Execute(IFormFile file, string path, string filename)
        {
           return _imageWriteOnlyRepository.Upload(file, path, filename);
        }

        
    }
}
