﻿using Lynx.ArcelorMittalVega.SGE.Application.Repositories;
using Lynx.ArcelorMittalVega.SGE.Domain.SystemConfigurations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Commands.UpdateConfigurations
{
    /// <summary>
    /// Classe <c>UpdateConfigurationsUseCase</c>
    /// <para>Classe responsável pelo caso de uso de cadastro, exclusão e edição de configurações</para>
    /// </summary>
    public class UpdateConfigurationsUseCase : IUpdateConfigurationsUseCase
    {
        private readonly IConfigurationWriteOnlyRepository _configurationWriteOnlyRepository;
        public UpdateConfigurationsUseCase(IConfigurationWriteOnlyRepository configurationWriteOnlyRepository)
        {
            _configurationWriteOnlyRepository = configurationWriteOnlyRepository;
        }
        /// <summary>
        /// Método que adiciona uma um parâmetro para ser visualizada e gerenciada pelo sistema.
        /// </summary>
        /// <param name="variableParameter">Um objeto de chave-valor com o nome do parâmetro e o nome da variável a ser inserida.</param>
        public void AddVariableParameter(SystemConfiguration variableParameter)
        {
            _configurationWriteOnlyRepository.AddVariableParameter(variableParameter);
        }

        /// <summary>
        /// Método que deleta um parâmetro da visualização do sistema. O parâmetro não é removido da base de dados.
        /// </summary>
        /// <param name="key">O nome do parâmetro a ser removido.</param>
        public void DeleteVariableParameter(string key)
        {
            _configurationWriteOnlyRepository.DeleteVariableParameter(key);
        }

        /// <summary>
        /// Método que atualiza as configurações salvas em batch.
        /// </summary>
        /// <param name="systemConfigurations">Lista de objetos com o valor a serem atualizados.</param>
        public void UpdateGeneralConfiguration(List<SystemConfiguration> systemConfigurations)
        {
            _configurationWriteOnlyRepository.UpdateGeneralConfigurations(systemConfigurations);
        }
    }
}
