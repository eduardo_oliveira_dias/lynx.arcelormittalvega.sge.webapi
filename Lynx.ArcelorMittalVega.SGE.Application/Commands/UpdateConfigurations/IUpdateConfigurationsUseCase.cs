﻿using Lynx.ArcelorMittalVega.SGE.Domain.SystemConfigurations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Commands.UpdateConfigurations
{
    public interface IUpdateConfigurationsUseCase
    {
        /// <summary>
        /// Método que atualiza as configurações salvas em batch.
        /// </summary>
        /// <param name="systemConfigurations">Lista de objetos com o valor a serem atualizados.</param>
        void UpdateGeneralConfiguration(List<SystemConfiguration> systemConfigurations);
        /// <summary>
        /// Método que deleta um parâmetro da visualização do sistema. O parâmetro não é removido da base de dados.
        /// </summary>
        /// <param name="key">O nome do parâmetro a ser removido.</param>
        void DeleteVariableParameter(string key);
        /// <summary>
        /// Método que adiciona uma um parâmetro para ser visualizada e gerenciada pelo sistema.
        /// </summary>
        /// <param name="variableParameter">Um objeto de chave-valor com o nome do parâmetro e o nome da variável a ser inserida.</param>
        void AddVariableParameter(SystemConfiguration variableParameter);
    }
}
