﻿using Lynx.ArcelorMittalVega.SGE.Domain.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Commands.ManageUsers
{
    public interface IUserLoginUseCase
    {
        /// <summary>
        /// Método que valida o usuário e senha passados consulta o AFW do IP21 para validar se o usuário está em uma das Roles definidas na
        /// procedure SGE_UserRoles e após isso gera o token de acesso ao sistema.
        /// </summary>
        /// <param name="user">Usuário a ser validado, serão utilizados os campos <value>Name</value> e <value>Password</value> da classe: <c>UserAD</c></param>
        /// <returns>Objeto do tipo <c>UserToken</c> com o Token e as informações do usuário</returns>
        /// <exception cref="Application.ApplicationException">Caso o usuário ou senha estiverem inválidos.</exception>
        /// <exception cref="Application.ApplicationException">Caso o usuário não esteja inserido em nenhuma role.</exception>
        UserToken Login(UserAD user);
    }
}
