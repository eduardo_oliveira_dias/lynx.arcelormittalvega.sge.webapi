﻿using Lynx.ArcelorMittalVega.SGE.Application.Repositories;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.Domain.Users;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Lynx.ArcelorMittalVega.SGE.Application.Commands.ManageUsers
{
    public class UserLoginUseCase : IUserLoginUseCase
    {
        private readonly IUserReadOnlyRepository _userReadonlyRepository;
        private readonly IConfiguration _configuration; 
        public UserLoginUseCase(IUserReadOnlyRepository userReadOnlyRepository, IConfiguration configuration)
        {
            _userReadonlyRepository = userReadOnlyRepository;
            _configuration = configuration;
        }
        /// <summary>
        /// Método que valida o usuário e senha passados consulta o AFW do IP21 para validar se o usuário está em uma das Roles definidas na
        /// procedure SGE_UserRoles e após isso gera o token de acesso ao sistema.
        /// </summary>
        /// <param name="user">Usuário a ser validado, serão utilizados os campos <value>Name</value> e <value>Password</value> da classe: <c>UserAD</c></param>
        /// <returns>Objeto do tipo <c>UserToken</c> com o Token e as informações do usuário</returns>
        /// <exception cref="Application.ApplicationException">Caso o usuário ou senha estiverem inválidos.</exception>
        /// <exception cref="Application.ApplicationException">Caso o usuário não esteja inserido em nenhuma role.</exception>
        public UserToken Login(UserAD user)
        {
            var userLogged = _userReadonlyRepository.Login(user);

            if (!userLogged)
            {
                throw new ApplicationException(MessageUser.InvalidUserNameOrPassword());
            }
            //user = _userReadonlyRepository.GetUserInfo(user); // verificar o motivo de não estar retornando o usuário no ambiente da AMV
            user.SamAccountName = user.Name;
            string[] roles = _userReadonlyRepository.GetUserRoles(user);

            if(roles.Length < 1)
            {
                throw new ApplicationException(MessageUser.UnauthorizedUser(user.SamAccountName));
            }
            user.Roles = roles;


            return GenerateToken(user);
        }
        /// <summary>
        /// Método responsável por gerar o Token JWT com os parametros de segurança do arquivo appsettings.json
        /// </summary>
        /// <param name="user">Usuário validado que gerará o token.</param>
        /// <returns>Objeto do tipo <c>UserToken</c> com o Token e as informações do usuário</returns>
        private UserToken GenerateToken(UserAD user)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, user.SamAccountName),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.SamAccountName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
            user.Password = "";

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expirationDate = DateTime.Now.AddHours(double.Parse(_configuration["TokenConfiguration:ExpirationHours"]));
            JwtSecurityToken token = new JwtSecurityToken(
                issuer: _configuration["TokenConfiguration:Issuer"],
                audience: _configuration["TokenConfiguration:Audience"],
                claims: claims,
                signingCredentials: credentials,
                expires: expirationDate
            );

            var generatedToken = new JwtSecurityTokenHandler().WriteToken(token);

            return new UserToken
            {
                Authenticated = true,
                Token = generatedToken,
                Expiration = expirationDate,
                Message = "Usuário logado",
                UserLogged = user
            };
        }
    }
}
