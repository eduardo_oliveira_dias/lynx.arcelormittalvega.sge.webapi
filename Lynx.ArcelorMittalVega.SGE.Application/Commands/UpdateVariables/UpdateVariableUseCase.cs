﻿using Lynx.ArcelorMittalVega.SGE.Application.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Application.Commands.UpdateVariables
{
    public class UpdateVariableUseCase : IUpdateVariableUseCase
    {
        private readonly IVariableWriteOnlyRepository _variableWriteOnlyRepository;
        public UpdateVariableUseCase(IVariableWriteOnlyRepository variableWriteOnlyRepository)
        {
            _variableWriteOnlyRepository = variableWriteOnlyRepository;
        }
        /// <summary>
        /// Método que atualiza o valor do parâmetro da variável.
        /// </summary>
        /// <param name="variableName">Nome da variável.</param>
        /// <param name="parameter">Nome do parâmetro.</param>
        /// <param name="value">Valor do parâmetro.</param>
        public void Update(string variableName, string parameter, object value)
        {
            //TODO: implementar tratamento de exceções
            _variableWriteOnlyRepository.Update(variableName, parameter, value);
        }
    }
}
