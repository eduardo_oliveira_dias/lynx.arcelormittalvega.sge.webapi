﻿using Dapper;
using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Domain.Variables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Queries
{
    /// <summary>
    /// Classe <c>VariableQueries</c>
    /// <para>Classe responsável pelas operações de consulta das variáveis disponíveis no sistema.</para>
    /// </summary>
    public class VariableQueries : IVariableQueries
    {
        private readonly string _connectionString;

        public VariableQueries(string connectionString)
        {
            _connectionString = connectionString;
        }
        /// <summary>
        /// Método que consulta a variável no IP21 por nome ou descrição.
        /// </summary>
        /// <param name="name">Nome a ser buscado.</param>
        /// <returns>Lista de variáveis que correspondem à busca.</returns>
        public List<Variable> GetVariables(string name)
        {
            using(IDbConnection db = new OdbcConnection(_connectionString))
            {
                string query = $"SELECT * from V_SGE_EquipmentVariables where (Name like '%{name}%' OR Description like '%{name}%')";

                List<Entities.Variable> variablesResult = db.Query<Entities.Variable>(query, commandTimeout:180).ToList();

                List<Variable> variables = variablesResult.Select(v => Variable.Load(v.Name,v.Description, v.Value, v.EquipmentName, v.EquipmentDescription, v.IPPlantArea, v.EngUnits, v.Quality, v.IP_High_High_Limit, v.IP_High_Limit, v.IP_Low_Low_Limit, v.IP_Low_Limit, v.Severity)).ToList();

                return variables;
            }
        }

        /// <summary>
        /// Método que consulta as variáveis do equipamento passado.
        /// </summary>
        /// <param name="equipmentName">nome do equipamento.</param>
        /// <returns>Lista de variáveis do equipamento.</returns>
        public List<Variable> GetEquipmentVariables(string equipmentName)
        {
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {
                string query = $"SELECT * from V_SGE_EquipmentVariables where EquipmentName = '{equipmentName}'";

                List<Entities.Variable> variablesResult = db.Query<Entities.Variable>(query, commandTimeout: 180).ToList();

                List<Variable> variables = variablesResult.Select(v => Variable.Load(v.Name, v.Description, v.Value, v.EquipmentName, v.EquipmentDescription, v.IPPlantArea, v.EngUnits, v.Quality, v.IP_High_High_Limit, v.IP_High_Limit, v.IP_Low_Low_Limit, v.IP_Low_Limit, v.Severity)).ToList();

                return variables;
            }
        }
    }
}
