﻿using Dapper;
using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Domain.Alarms;
using Lynx.ArcelorMittalVega.SGE.Domain.Graphics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Text;
using System.Linq;
using System.Globalization;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using Lynx.ArcelorMittalVega.SGE.Application.Results;
using Microsoft.Extensions.Caching.Memory;
using Lynx.ArcelorMittalVega.SGE.Domain.Alarms.Enums;
using Lynx.ArcelorMittalVega.SGE.Application.LogManager;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Queries
{
    /// <summary>
    /// Classe <c>GraphicQueries</c>
    /// <para>Classe responsável pelas operações de consulta de dados de alarmes para montagem dos gráficos da aplicação.</para>
    /// </summary>
    public class GraphicQueries : IGraphicQueries
    {
        private string _connectionString;
        private readonly CultureInfo cultureInfo = CultureInfo.GetCultureInfo("en");
        private readonly string datePattern = "dd-MMM-yy HH:mm:ss";
        private IMemoryCache _cache;

        public GraphicQueries(string connectionString, IMemoryCache cache)
        {
            _connectionString = connectionString;
            _cache = cache;
        }

        /// <summary>
        /// Método que consulta os alarmes ativos e estrutura os dados para montar os gráficos de setor(pizza) para os níveis da hierarquia.
        /// </summary>
        /// <param name="childrenLevel">O nível filho do nível que está sendo mostrado, para montagem dos gráficos agrupando por nível. 
        /// Ex.: O nível Segmento tem como nível filho as áreas.</param>
        /// <param name="parentLevel">Nível que está sendo mostrado.</param>
        /// <param name="parentLevelName">Nome do nível que está sendo mostrado.</param>
        /// <returns>Lista de gráfico de setor para cada filho do nível.</returns>
        public IEnumerable<GraphicSector> GetAlarmsGraphicSector(string childrenLevel, string parentLevel, string parentLevelName)
        {
            List<GraphicSector> result = new List<GraphicSector>();
            
            using (OdbcConnection db = new OdbcConnection(_connectionString))
            {
                db.ConnectionTimeout = 180;
                string queryAlarms = $"select * from v_sge_alarms";


                List<Entities.Alarm> alarmsResult = db.Query<Entities.Alarm>(queryAlarms, commandTimeout: 180).AsList();
                List<Entities.Alarms_Equipment> equipments = GetAlarmEquipments().ToList();

                alarmsResult.ForEach(x =>
                {
                    SetEquipments(x, equipments);
                });

                List<Alarm> alarms = alarmsResult.Where(x => x.EquipmentName != null).Select(alarm => Alarm.Load(alarm.VariableName, alarm.VariableDescription, alarm.EngUnit, alarm.Value, alarm.TimeStamp, alarm.EquipmentName, alarm.EquipmentDescription, alarm.AreaName, alarm.AreaDescription, alarm.SectionName, alarm.SectionDescription, alarm.SegmentName, alarm.SegmentDescription, alarm.Severity)).ToList();
                List<IGrouping<string, Alarm>> alarmsByVariable = alarms.GroupBy(x => x.VariableName).ToList();
                alarms = alarmsByVariable.Select(x => x.OrderByDescending(y => y.TimeStamp).FirstOrDefault()).ToList();

                alarms = FilterAlarmsByLevel(alarms, parentLevel, parentLevelName);
                result = MountGraphicLevels(alarms, childrenLevel, parentLevelName).GraphicData;
                return result;
            }
        }
        /// <summary>
        /// Método responsável por consultar os dados históricos de uma lista de variáveis e estruturar os dados para o gráfico de tendência
        /// (linha).
        /// </summary>
        /// <param name="variableNames">A lista de nomes das variáveis</param>
        /// <param name="startTime">Data início da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <returns>A lista de dados de gráfico de tendência.</returns>
        public IEnumerable<GraphicTrend> GetAlarmsGraphicTrend(List<string> variableNames, DateTime startTime, DateTime endTime)
        {
            startTime = startTime.ToLocalTime();
            endTime = endTime.ToLocalTime();
            if (startTime > endTime)
            {
                throw new Application.ApplicationException(MessageUser.StartTimeGreaterThanEndTime());
            }
            if (endTime.ToLocalTime() > DateTime.Now)
            {
                throw new Application.ApplicationException(MessageUser.EndTimeGreaterThanCurrentDate());
            }
            using (OdbcConnection db = new OdbcConnection(_connectionString))
            {
                db.ConnectionTimeout = 180;
                List<GraphicTrend> listGraphicTrend = new List<GraphicTrend>();
                foreach (string variableName in variableNames)
                {
                    string query = $"SELECT * FROM V_SGE_AlarmsTrendDetail WHERE variableName like '{variableName}%' AND \"timestamp\" BETWEEN '{startTime.ToString("dd-MMM-yy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"))}' AND '{endTime.ToString("dd-MMM-yy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"))}'";
                    
                    List<Entities.Alarm> alarmsResult = db.Query<Entities.Alarm>(query, commandTimeout: 1800).AsList();
                    alarmsResult = alarmsResult.Where(x => x.VariableName.Contains(variableName)).ToList();

                    if (alarmsResult.Any())
                    {
                        List<Alarm> alarms = alarmsResult.Select(alarm => Alarm.Load(alarm.VariableName, alarm.VariableDescription, alarm.EngUnit, alarm.Value, alarm.TimeStamp)).ToList();

                        GraphicTrend graphicTrend = new GraphicTrend();
                        graphicTrend.Info = alarms.Select(x => x.VariableDescription ?? x.VariableName).FirstOrDefault();
                        graphicTrend.Description = alarms.Select(x => x.VariableDescription).FirstOrDefault();
                        foreach (Alarm alarm in alarms.OrderBy(x => x.TimeStamp))
                        {
                            GraphicDataItem item = new GraphicDataItem();
                            item.Name = alarm.TimeStamp.ToString("dd/MM/yyyy HH:mm:ss");
                            item.Value = alarm.Value;

                            graphicTrend.Items.Add(item);
                        }
                        listGraphicTrend.Add(graphicTrend);

                    }
                }

                return listGraphicTrend;
            }
        }

        /// <summary>
        /// Método para trazer os alarmes por tipo em formato de gráfico de setor(pizza), dentro do período passado.
        /// </summary>
        /// <param name="startTime">Data início da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <param name="level">Nível da hierarquia para agrupar os alarmes.</param>
        /// <param name="levelName">Nome do nível para filtrar os alarmes.</param>
        /// <returns>Um objeto com os dados estruturados em gráfico de setor(pizza), junto a lista de alarmes encontrados.</returns>
        public GraphicResult<GraphicSector> GetAlarmsGraphicType(DateTime startTime, DateTime endTime, Level level, string levelName)
        {
            GraphicResult<GraphicSector> graphicResult = new GraphicResult<GraphicSector>();

            startTime = startTime.ToLocalTime();
            endTime = endTime.ToLocalTime();
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {
                HashSet<GraphicSector> graphicList = new HashSet<GraphicSector>();
                Dictionary<string, GraphicSector> dictionaryGraphic = new Dictionary<string, GraphicSector>();

                //string query = $"SELECT * FROM V_SGE_AlarmsTrendDetail WHERE \"timestamp\" BETWEEN '{startTime.ToString("dd-MMM-yy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"))}' AND '{endTime.ToString("dd-MMM-yy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"))}'";
                string query = $"SGE_SP_AlarmsTrend('{startTime.ToString(datePattern, cultureInfo)}', '{endTime.ToString(datePattern, cultureInfo)}')";

                List<Entities.Alarm> alarmsResult = db.Query<Entities.Alarm>(query, commandTimeout: 1800).AsList();
                List<Entities.Alarms_Equipment> equipments = GetAlarmEquipments().ToList();

                alarmsResult.ForEach(x =>
                {
                    SetEquipments(x, equipments);
                });
                List<Alarm> alarms = alarmsResult.Where(x => x.EquipmentName != null).Select(alarm => Alarm.Load(alarm.VariableName, alarm.VariableDescription, alarm.EngUnit, alarm.Value, alarm.TimeStamp, alarm.EquipmentName, alarm.EquipmentDescription, alarm.AreaName, alarm.AreaDescription, alarm.SectionName, alarm.SectionDescription, alarm.SegmentName, alarm.SegmentDescription, alarm.Severity)).ToList();
                alarms = alarms.Where(x => x.AlarmSeverity != AlarmSeverity.NORMAL).ToList();
                List<IGrouping<string, Alarm>> alarmsByVariable = alarms.GroupBy(x => x.VariableName).ToList();
                alarms = alarmsByVariable.Select(x => x.OrderBy(x => x.AlarmSeverity).ThenByDescending(x => x.TimeStamp).FirstOrDefault()).ToList();

                alarms = FilterAlarmsByLevel(alarms, level, levelName);

                GraphicSector graphic = new GraphicSector();
                graphic.Info = "Alarmes por Tipo";
                graphic.Description = "";

                List<IGrouping<string, Alarm>> groups = alarms.GroupBy(x => x.Severity).ToList();

                foreach (var group in groups)
                {
                    GraphicDataItem item = new GraphicDataItem();
                    item.Name = group.Key;
                    item.Value = group.Count();

                    graphic.Items.Add(item);

                }
                graphicResult.Alarms = alarms.OrderBy(x => x.AlarmSeverity).ThenByDescending(x => x.TimeStamp).ToList();
                graphicResult.GraphicData = graphic;

                return graphicResult;
            }
        
        }

        /// <summary>
        /// Método responsável pela pesquisa de alarmes agrupando por nível e depois por tipo, para cada nível da hierarquia. Os dados serão
        /// estruturados em formato de barra.
        /// </summary>
        /// <param name="startTime">Data início da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <param name="level">Nível da hierarquia para agrupar os alarmes.</param>
        /// <param name="levelName">Nome do nível para filtrar os alarmes.</param>
        /// <returns>Um objeto com os dados estruturados em gráfico de barra, junto a lista de alarmes encontrados.</returns>
        public GraphicResult<List<GraphicBar>> GetAlarmsGraphicBar(DateTime startTime, DateTime endTime, Level level, string levelName)
        {
            GraphicResult<List<GraphicBar>> result = new GraphicResult<List<GraphicBar>>();
            startTime = startTime.ToLocalTime();
            endTime = endTime.ToLocalTime();
            using (OdbcConnection db = new OdbcConnection(_connectionString))
            {
                db.ConnectionTimeout = 180;
                //string query = $"SELECT * FROM V_SGE_AlarmsTrendDetail WHERE \"timestamp\" BETWEEN '{startTime.ToString("dd-MMM-yy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"))}' AND '{endTime.ToString("dd-MMM-yy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"))}'";
                string query = $"SGE_SP_AlarmsTrend('{startTime.ToString(datePattern, cultureInfo)}', '{endTime.ToString(datePattern, cultureInfo)}')";

                List<Entities.Alarm> alarmsResult = db.Query<Entities.Alarm>(query, commandTimeout: 1800).AsList();
                LxLogger.Info($"Count AlarmsResult : {alarmsResult.Count}");

                List<Entities.Alarms_Equipment> equipments = GetAlarmEquipments().ToList();
                LxLogger.Info($"Count Equipments : {equipments.Count}");

                alarmsResult.ForEach(x =>
                {
                    SetEquipments(x, equipments);
                });

                List<Alarm> alarms = alarmsResult.Where(x => x.EquipmentName != null).Select(alarm => Alarm.Load(alarm.VariableName, alarm.VariableDescription, alarm.EngUnit, alarm.Value, alarm.TimeStamp, alarm.EquipmentName, alarm.EquipmentDescription, alarm.AreaName, alarm.AreaDescription, alarm.SectionName, alarm.SectionDescription, alarm.SegmentName, alarm.SegmentDescription, alarm.Severity)).ToList();
                alarms = alarms.Where(x => x.AlarmSeverity != AlarmSeverity.NORMAL).ToList();

                List<IGrouping<string, Alarm>> alarmsByVariable = alarms.GroupBy(x => x.VariableName).ToList();

                alarms = alarmsByVariable.Select(x => x.OrderBy(x => x.AlarmSeverity).ThenByDescending(x => x.TimeStamp).FirstOrDefault()).ToList();

                result = MountGraphicBar(alarms.OrderBy(x => x.AlarmSeverity).ThenByDescending(x => x.TimeStamp).ToList(), level, levelName);
                return result;
            }
        }

        /// <summary>
        /// Método responsável por montar o gráfico de barras apartir de uma lista de alarmes agrupando por nível e depois por tipo, 
        /// para cada nível da hierarquia.
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem tratados.</param>
        /// <param name="level">Nível da hierarquia para agrupar os alarmes.</param>
        /// <param name="levelName">Nome do nível para filtrar os alarmes.</param>
        /// <returns>Um objeto com os dados estruturados em gráfico de barra, junto a lista de alarmes encontrados.</returns>
        public GraphicResult<List<GraphicBar>> MountGraphicBar(List<Alarm> alarms, Level level, string levelName)
        {
            GraphicResult<List<GraphicBar>> result = new GraphicResult<List<GraphicBar>>();
            List<GraphicBar> graphics = new List<GraphicBar>();

            alarms = FilterAlarmsByLevel(alarms, level, levelName);
            List<IGrouping<string, Alarm>> groups = GetGroupingBase(alarms, level);
            foreach (var group in groups)
            {
                GraphicBar graphic = new GraphicBar();
                graphic.Info = group.Key;
                graphic.Description = GetDescription(alarms, group.Key, level);
                List<IGrouping<string, Alarm>> subGroups = group.GroupBy(x => x.Severity).ToList();

                foreach (var subGroup in subGroups)
                {
                    GraphicDataItem item = new GraphicDataItem();
                    item.Name = subGroup.Key;
                    item.Value = subGroup.Count();

                    graphic.Items.Add(item);

                }

                graphics.Add(graphic);
            }
            result.GraphicData = graphics;
            result.Alarms = alarms;

            return result;

        }

        /// <summary>
        /// Método responsável por montar o gráfico de setor(pizza) a partir de uma lista de alarmes agrupando por tipo para cada nível da hierarquia.
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem tratados.</param>
        /// <param name="level">Nível da hierarquia para agrupar os alarmes.</param>
        /// <param name="levelName">Nome do nível para filtrar os alarmes.</param>
        /// <returns>Um objeto com os dados estruturados em gráfico de setor(pizza), junto a lista de alarmes encontrados.</returns>
        public GraphicResult<List<GraphicSector>> MountGraphicLevels(List<Alarm> alarms, string level, string levelName)
        {
            GraphicResult<List<GraphicSector>> result = new GraphicResult<List<GraphicSector>>();
            List<GraphicSector> graphics = new List<GraphicSector>();
            
            
            List<IGrouping<string, Alarm>> groups = GetGroupingBase(alarms, level);
            foreach (var group in groups)
            {
                GraphicSector graphic = new GraphicSector();
                graphic.Info = group.Key;
                graphic.Description = GetDescription(alarms, group.Key, level);
                List<IGrouping<string, Alarm>> subGroups = group.GroupBy(x => x.Severity).ToList();

                foreach (var subGroup in subGroups)
                {
                    GraphicDataItem item = new GraphicDataItem();
                    item.Name = subGroup.Key;
                    item.Value = subGroup.Count();

                    graphic.Items.Add(item);

                }

                graphics.Add(graphic);
            }
            result.GraphicData = graphics;
            result.Alarms = alarms;

            return result;

        }

        /// <summary>
        /// Método responsável por montar o gráfico de setor(pizza) a partir de uma lista de alarmes agrupando por tipo para cada nível da hierarquia.
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem tratados.</param>
        /// <param name="level">Nível da hierarquia para agrupar os alarmes.</param>
        /// <param name="levelName">Nome do nível para filtrar os alarmes.</param>
        /// <returns>Um objeto com os dados estruturados em gráfico de setor(pizza), junto a lista de alarmes encontrados.</returns>
        public GraphicResult<GraphicSector> MountGraphicType(List<Alarm> alarms, Level level, string levelName)
        {
            GraphicResult<GraphicSector> graphicResult = new GraphicResult<GraphicSector>();
            List<GraphicBar> graphics = new List<GraphicBar>();

            alarms = FilterAlarmsByLevel(alarms, level, levelName);
            GraphicSector graphic = new GraphicSector();
            graphic.Info = "Alarmes por Tipo";
            graphic.Description = "";

            List<IGrouping<string, Alarm>> groups = alarms.GroupBy(x => x.Severity).ToList();

            foreach (var group in groups)
            {
                GraphicDataItem item = new GraphicDataItem();
                item.Name = group.Key;
                item.Value = group.Count();

                graphic.Items.Add(item);

            }
            graphicResult.Alarms = alarms;
            graphicResult.GraphicData = graphic;

            return graphicResult;
        }

        #region Auxiliares
        /// <summary>
        /// Método que estrutura os alarmes em grupos, a partir do nível passado
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem agrupados.</param>
        /// <param name="level">O nível da hierarquia a ser usado como parametro de agrupamento.</param>
        /// <returns>Uma lista de alarmes agrupados.</returns>
        private List<IGrouping<string, Alarm>> GetGroupingBase(List<Alarm> alarms, Level level)
        {
            List<IGrouping<string, Alarm>> groups;
            switch (level)
            {
                case Level.Segment:
                    groups = alarms.GroupBy(x => x.AreaName).ToList();
                    break;
                case Level.Area:
                    groups = alarms.GroupBy(x => x.SectionName).ToList();
                    break;
                case Level.Section:
                    groups = alarms.GroupBy(x => x.EquipmentName).ToList();
                    break;
                default:
                    groups = alarms.GroupBy(x => x.SegmentName).ToList();
                    break;
            }

            return groups;
        }

        /// <summary>
        /// Método que estrutura os alarmes em grupos, a partir do nível filho passado.
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem agrupados.</param>
        /// <param name="childrenLevel">O nível filho da hierarquia a ser usado como parametro de agrupamento.</param>
        /// <returns>Uma lista de alarmes agrupados.</returns>
        private List<IGrouping<string, Alarm>> GetGroupingBase(List<Alarm> alarms, string childrenLevel)
        {
            List<IGrouping<string, Alarm>> groups;
            switch (childrenLevel)
            {
                case "segmentName":
                    groups = alarms.GroupBy(x => x.SegmentName).ToList();
                    break;
                case "areaName":
                    groups = alarms.GroupBy(x => x.AreaName).ToList();
                    break;
                case "sectionName":
                    groups = alarms.GroupBy(x => x.SectionName).ToList();
                    break;
                case "equipmentName":
                    groups = alarms.GroupBy(x => x.EquipmentName).ToList();
                    break;
                default:
                    groups = alarms.GroupBy(x => x.SegmentName).ToList();
                    break;
            }

            return groups;
        }

        /// <summary>
        /// Filtra os alarmes por nível.
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem filtrados.</param>
        /// <param name="level">O nível a ser filtrado.</param>
        /// <param name="levelName">O nome do nível que servirá de filtro.</param>
        /// <returns>A lista de alarmes filtrados.</returns>
        private List<Alarm> FilterAlarmsByLevel(List<Alarm> alarms, Level level, string levelName)
        {
            
            switch (level)
            {
                case Level.Segment:
                    alarms = alarms.Where(x => x.SegmentName == levelName).ToList();
                    break;
                case Level.Area:
                    alarms = alarms.Where(x => x.AreaName == levelName).ToList();
                    break;
                case Level.Section:
                    alarms = alarms.Where(x => x.SectionName == levelName).ToList();
                    break;
                case Level.Equipment:
                    alarms = alarms.Where(x => x.EquipmentName == levelName).ToList();
                    break;
            }

            return alarms;
        }

        /// <summary>
        /// Filtra os alarmes por nível.
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem filtrados.</param>
        /// <param name="level">O nível a ser filtrado.</param>
        /// <param name="levelName">O nome do nível que servirá de filtro.</param>
        /// <returns>A lista de alarmes filtrados.</returns>
        private List<Alarm> FilterAlarmsByLevel(List<Alarm> alarms, string level, string levelName)
        {

            switch (level)
            {
                case "segmentName":
                    alarms = alarms.Where(x => x.SegmentName == levelName).ToList();
                    break;
                case "areaName":
                    alarms = alarms.Where(x => x.AreaName == levelName).ToList();
                    break;
                case "sectionName":
                    alarms = alarms.Where(x => x.SectionName == levelName).ToList();
                    break;
                case "equipmentName":
                    alarms = alarms.Where(x => x.EquipmentName == levelName).ToList();
                    break;
            }

            return alarms;
        }

        /// <summary>
        /// Retorna a descrição do nível agrupado, a partir da lista passada.
        /// </summary>
        /// <param name="alarms">Lista de alarmes para pesquisa da descrição.</param>
        /// <param name="groupKey">A chave do grupo, que é o nome do nível agrupado.</param>
        /// <param name="level">O nível da hierarquia</param>
        /// <returns>A string com a descrição.</returns>
        private string GetDescription(List<Alarm> alarms, string groupKey, Level level)
        {
            string description = "";
            

            switch (level)
            {
                case Level.Segment:
                    description = alarms.FirstOrDefault(x => (x.AreaName == groupKey && !String.IsNullOrEmpty(x.AreaDescription)))?.AreaDescription;
                    break;
                case Level.Area:
                    description = alarms.FirstOrDefault(x => (x.SectionName == groupKey && !String.IsNullOrEmpty(x.SectionDescription)))?.SectionDescription;
                    break;
                case Level.Section:
                    description = alarms.FirstOrDefault(x => (x.EquipmentName == groupKey && !String.IsNullOrEmpty(x.EquipmentDescription)))?.EquipmentDescription;
                    break;
                default:
                    description = alarms.FirstOrDefault(x => (x.SegmentName == groupKey && !String.IsNullOrEmpty(x.SegmentDescription)))?.SegmentDescription;
                    break;
            }

            return description;
        }

        /// <summary>
        /// Retorna a descrição do nível agrupado, a partir da lista passada.
        /// </summary>
        /// <param name="alarms">Lista de alarmes para pesquisa da descrição.</param>
        /// <param name="groupKey">A chave do grupo, que é o nome do nível agrupado.</param>
        /// <param name="level">O nível da hierarquia</param>
        /// <returns>A string com a descrição.</returns>
        private string GetDescription(List<Alarm> alarms, string groupKey, string level)
        {
            string description = "";


            switch (level)
            {
                case "areaName":
                    description = alarms.FirstOrDefault(x => (x.AreaName == groupKey && !String.IsNullOrEmpty(x.AreaDescription)))?.AreaDescription;
                    break;
                case "sectionName":
                    description = alarms.FirstOrDefault(x => (x.SectionName == groupKey && !String.IsNullOrEmpty(x.SectionDescription)))?.SectionDescription;
                    break;
                case "equipmentName":
                    description = alarms.FirstOrDefault(x => (x.EquipmentName == groupKey && !String.IsNullOrEmpty(x.EquipmentDescription)))?.EquipmentDescription;
                    break;
                default:
                    description = alarms.FirstOrDefault(x => (x.SegmentName == groupKey && !String.IsNullOrEmpty(x.SegmentDescription)))?.SegmentDescription;
                    break;
            }

            return description;
        }

        /// <summary>
        /// Método responsável pela consulta e cache das variáveis ativas para cada equipamento na base de dados, para fazer a 
        /// atribuição do equipamento à variável e preencher as informações de localização: Segmento, Área e Seção que o Equipamento
        /// está.
        /// </summary>
        /// <returns>A lista de variáveis e seus respectivos equipamentos, com as
        /// informações de localização: Segmento, Área e Seção que o Equipamento.</returns>
        private IEnumerable<Entities.Alarms_Equipment> GetAlarmEquipments()
        {
            List<Entities.Alarms_Equipment> results;
            if (_cache.TryGetValue("_equipmentsList", out object alarmsEquipmentAux))
            {
                results = (List<Entities.Alarms_Equipment>)alarmsEquipmentAux;
            }
            else
            {
                using (IDbConnection db = new OdbcConnection(_connectionString))
                {
                    string query = "SELECT * FROM V_SGE_AlarmsFolder";
                    string querySections = "SELECT chave name, valor description from SGE_Sections where name <> ''";
                    string queryAreas = "SELECT chave name, valor description from SGE_Areas where name <> ''";
                    string querySegments = "SELECT chave name, valor description from SGE_Segments where name <> ''";

                    results = db.Query<Entities.Alarms_Equipment>(query, commandTimeout: 180).ToList();

                    var sections = db.Query<Entities.PlantLevel>(querySections, commandTimeout: 180).ToList();
                    var areas = db.Query<Entities.PlantLevel>(queryAreas, commandTimeout: 180).ToList();
                    var segments = db.Query<Entities.PlantLevel>(querySegments, commandTimeout: 180).ToList();

                    foreach (var equipment in results)
                    {
                        var section = sections.FirstOrDefault(s => equipment.EquipmentName.Contains(s.Name));
                        if (section != null)
                        {
                            equipment.SectionName = section.Name;
                            equipment.SectionDescription = section.Description;
                        }

                        var area = areas.FirstOrDefault(a => equipment.EquipmentName.Contains(a.Name));
                        if (area != null)
                        {
                            equipment.AreaName = area.Name;
                            equipment.AreaDescription = area.Description;
                        }

                        var segment = segments.FirstOrDefault(seg => equipment.EquipmentName.Contains(seg.Name));
                        if (segment != null)
                        {
                            equipment.SegmentName = segment.Name;
                            equipment.SegmentDescription = segment.Description;
                        }
                    }
                    
                    _cache.Set("_equipmentsList", results, new DateTimeOffset(DateTime.Today.AddDays(1)));
                }

            }
            //LxLogger.Info($"Equipments: {JsonConvert.SerializeObject(results.Select(x => new { x.EquipmentName, x.Record_name }))}");
            return results;
        }

        /// <summary>
        /// Método que atribui a variável do alarme ao equipamento.
        /// </summary>
        /// <param name="alarm">Alarme que terá o equipamento atribuído através do record_name.</param>
        /// <param name="equipments">Lista de nomes de equipamentos com as variáveis para atribuir.</param>
        private void SetEquipments(Entities.Alarm alarm, List<Entities.Alarms_Equipment> equipments)
        {
            Entities.Alarms_Equipment equipment = equipments.FirstOrDefault(e => e.Record_name == alarm.VariableName);
            if (equipment != null)
            {
                alarm.EquipmentName = equipment?.EquipmentName;
                alarm.EquipmentDescription = equipment.Description;
                alarm.SectionName = equipment.SectionName;
                alarm.SectionDescription = equipment.SectionDescription;
                alarm.SegmentName = equipment.SegmentName;
                alarm.SegmentDescription = equipment.SegmentDescription;
                alarm.AreaName = equipment.AreaName;
                alarm.AreaDescription = equipment.AreaDescription;
            }
        }

        #endregion
    }
}
