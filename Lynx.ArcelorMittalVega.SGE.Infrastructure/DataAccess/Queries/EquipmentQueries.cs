﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Domain.Equipments;
using Lynx.ArcelorMittalVega.SGE.Domain.Variables;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using Dapper;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Queries
{
    /// <summary>
    /// Classe <c>EquipmentQueries</c>
    /// <para>Classe responsável pelas operações de consulta de equipamentos no IP21</para>
    /// </summary>
    public class EquipmentQueries : IEquipmentQueries
    {
        private readonly string _connectionString;
        public EquipmentQueries(string connectionString)
        {
            _connectionString = connectionString;
        }
        /// <summary>
        /// Consulta todos os equipamentos na árvore SAP VIEW
        /// </summary>
        /// <returns><c>List<Equipment></c>Lista de Equipamentos encontrados.</returns>
        public List<Equipment> GetAll()
        {
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {

                string query = $"SELECT * from V_SGE_Tree";

                List<Entities.Equipment> equipmentsResult = db.Query<Entities.Equipment>(query, commandTimeout: 180).ToList();

                List<Equipment> equipments = equipmentsResult.Select(v => Equipment.Load(v.Name, v.Description)).ToList();

                return equipments;
            }
        }
        /// <summary>
        /// Consulta o equipamento na árvore filtrando por nome.
        /// </summary>
        /// <param name="name">Nome do Equipamento.</param>
        /// <returns><c>Equipment</c> Equipamento encontrado.</returns>
        public Equipment GetEquipment(string name)
        {
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {

                string query = $"SELECT name, description from FolderDef where name = '{name}'";

                Entities.Equipment equipmentResult = db.Query<Entities.Equipment>(query, commandTimeout: 180).FirstOrDefault();

                query = $"SELECT * from V_SGE_EquipmentVariables where EquipmentName = '{equipmentResult.Name}'";

                List<Entities.Variable> variablesResult = db.Query<Entities.Variable>(query, commandTimeout: 180).ToList();

                List<Variable> variables = variablesResult.Select(v => Variable.Load(v.Name, v.Description, v.Value, v.EquipmentName, v.EquipmentDescription, v.IPPlantArea, v.EngUnits, v.Quality, v.IP_High_High_Limit, v.IP_High_Limit, v.IP_Low_Low_Limit, v.IP_Low_Limit, v.IP_Graph_Maximum, v.IP_Graph_Minimum, v.Severity)).ToList();

                Equipment equipment = Equipment.Load(equipmentResult.Name, equipmentResult.Description, variables);

                return equipment;
            }
        }
    }
}
