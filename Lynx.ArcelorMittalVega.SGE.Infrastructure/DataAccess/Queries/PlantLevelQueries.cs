﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Application.Results;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Linq;
using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using Microsoft.Extensions.Caching.Memory;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Queries
{
    /// <summary>
    /// Classe <c>PlantLevelQueries</c>
    /// <para>Classe responsável pelas operações de consulta dos níveis hierárquicos na árvore SAP VIEW do IP21.</para>
    /// </summary>
    public class PlantLevelQueries : IPlantLevelQueries
    {
        public readonly string _connectionString;
        public IMemoryCache _cache;
        public PlantLevelQueries(string connectionString, IMemoryCache cache)
        {
            _connectionString = connectionString;
            _cache = cache;
        }

        /// <summary>
        /// Método responsável por consultar os níveis(folders) e seus respectivos pais, para montar a árvore.
        /// </summary>
        /// <returns>O objeto com a árvore montada.</returns>
        public PlantLevel GetPlantLevels()
        {
            using (OdbcConnection db = new OdbcConnection(_connectionString))
            {
                db.ConnectionTimeout = 180;
                string plantLevelQuery = "select * from V_SGE_PlantLevels";
                string alarmsQuery = "select * from v_sge_alarms where Severity = 'CRITICO'";

                List<Entities.PlantLevel> plantLevels = db.Query<Entities.PlantLevel>(plantLevelQuery, commandTimeout:180).ToList();
                List<Entities.Alarm> alarmsCritical = db.Query<Entities.Alarm>(alarmsQuery, commandTimeout: 180).ToList();
                List<Entities.Alarms_Equipment> equipments = GetAlarmEquipments().ToList();

                alarmsCritical.ForEach(x => SetEquipments(x, equipments));

                plantLevels.ForEach(x => x.AlarmCount = alarmsCritical.Count(a => a.EquipmentName == x.Name));

                List<PlantLevel> listPlantLevels = plantLevels.Select(plantLevel => PlantLevel.Load(plantLevel.Name, plantLevel.Description, plantLevel.Level, plantLevel.AlarmCount, plantLevel.ParentName)).ToList();

                return PlantLevel.LoadTree(listPlantLevels);

            }
        }

        /// <summary>
        /// Método que pequisa os níveis no IP21 a partir do nome ou  descrição.
        /// </summary>
        /// <param name="query">Nome ou descrição a ser buscada.</param>
        /// <returns>Lista dos níveis encontrados</returns>
        public List<PlantLevel> GetPlantLevels(string query)
        {
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {

                string command = $"SELECT * from V_SGE_Tree where name like'%{query}%' OR description like '%{query}%'";

                List<Entities.PlantLevel> equipmentsResult = db.Query<Entities.PlantLevel>(command, commandTimeout: 180).ToList();

                List<PlantLevel> plantLevels = equipmentsResult.Select(v => PlantLevel.Load(v.Name, v.Description)).ToList();

                return plantLevels;
            }
        }

        /// <summary>
        /// Método responsável por consultar determinados níveis da hierarquia a partir do nome e do nome do nível pai.
        /// </summary>
        /// <param name="levelName">Nome do nível desejado</param>
        /// <param name="parentName">Nome do nível pai</param>
        /// <returns>Lista dos níveis encontrados</returns>
        public List<PlantLevel> GetPlantLevelsByLevelName(string levelName, string parentName)
        {
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {

                string parentFilter = !String.IsNullOrEmpty(parentName) ? $" AND folder_parent = '{parentName}'" : "";

                string command = $"SELECT DISTINCT equipmentName name, description from V_SGE_AlarmsFolder where \"Level\" = '{levelName}'"+parentFilter;


                List<Entities.PlantLevel> equipmentsResult = db.Query<Entities.PlantLevel>(command, commandTimeout: 180).ToList();

                List<PlantLevel> plantLevels = equipmentsResult.Select(v => PlantLevel.Load(v.Name, v.Description)).ToList();

                return plantLevels;
            }
        }

        /// <summary>
        /// Método responsável pela consulta e cache das variáveis ativas para cada equipamento na base de dados, para fazer a 
        /// atribuição do equipamento à variável e preencher as informações de localização: Segmento, Área e Seção que o Equipamento
        /// está.
        /// </summary>
        /// <returns>A lista de variáveis e seus respectivos equipamentos, com as
        /// informações de localização: Segmento, Área e Seção que o Equipamento.</returns>
        private IEnumerable<Entities.Alarms_Equipment> GetAlarmEquipments()
        {
            List<Entities.Alarms_Equipment> results;
            if (_cache.TryGetValue("_equipmentsList", out object alarmsEquipmentAux))
            {
                results = (List<Entities.Alarms_Equipment>)alarmsEquipmentAux;
            }
            else
            {
                using (IDbConnection db = new OdbcConnection(_connectionString))
                {
                    string query = "SELECT * FROM V_SGE_AlarmsFolder";
                    string querySections = "SELECT chave name, valor description from SGE_Sections where name <> ''";
                    string queryAreas = "SELECT chave name, valor description from SGE_Areas where name <> ''";
                    string querySegments = "SELECT chave name, valor description from SGE_Segments where name <> ''";

                    results = db.Query<Entities.Alarms_Equipment>(query, commandTimeout: 180).ToList();

                    var sections = db.Query<Entities.PlantLevel>(querySections, commandTimeout: 180).ToList();
                    var areas = db.Query<Entities.PlantLevel>(queryAreas, commandTimeout: 180).ToList();
                    var segments = db.Query<Entities.PlantLevel>(querySegments, commandTimeout: 180).ToList();

                    foreach (var equipment in results)
                    {
                        var section = sections.FirstOrDefault(s => equipment.EquipmentName.Contains(s.Name));
                        if (section != null)
                        {
                            equipment.SectionName = section.Name;
                            equipment.SectionDescription = section.Description;
                        }

                        var area = areas.FirstOrDefault(a => equipment.EquipmentName.Contains(a.Name));
                        if (area != null)
                        {
                            equipment.AreaName = area.Name;
                            equipment.AreaDescription = area.Description;
                        }

                        var segment = segments.FirstOrDefault(seg => equipment.EquipmentName.Contains(seg.Name));
                        if (segment != null)
                        {
                            equipment.SegmentName = segment.Name;
                            equipment.SegmentDescription = segment.Description;
                        }
                    }

                    _cache.Set("_equipmentsList", results, new DateTimeOffset(DateTime.Today.AddDays(1)));
                }

            }

            return results;
        }

        /// <summary>
        /// Método que atribui a variável do alarme ao equipamento.
        /// </summary>
        /// <param name="alarm">Alarme que terá o equipamento atribuído através do record_name.</param>
        /// <param name="equipments">Lista de nomes de equipamentos com as variáveis para atribuir.</param>
        private void SetEquipments(Entities.Alarm alarm, List<Entities.Alarms_Equipment> equipments)
        {
            Entities.Alarms_Equipment equipment = equipments.FirstOrDefault(e => e.Record_name == alarm.VariableName);
            if (equipment != null)
            {
                alarm.EquipmentName = equipment?.EquipmentName;
                alarm.EquipmentDescription = equipment.Description;
                alarm.SectionName = equipment.SectionName;
                alarm.SectionDescription = equipment.SectionDescription;
                alarm.SegmentName = equipment.SegmentName;
                alarm.SegmentDescription = equipment.SegmentDescription;
                alarm.AreaName = equipment.AreaName;
                alarm.AreaDescription = equipment.AreaDescription;
            }
        }
    }
}
