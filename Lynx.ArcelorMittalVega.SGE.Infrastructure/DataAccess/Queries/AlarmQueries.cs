﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Domain.Alarms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Text;
using Dapper;
using System.Linq;
using System.Globalization;
using Lynx.ArcelorMittalVega.SGE.Application.Util;
using Microsoft.Extensions.Caching.Memory;
using Lynx.ArcelorMittalVega.SGE.Domain.Alarms.Enums;
using Lynx.ArcelorMittalVega.SGE.Application.LogManager;
using Newtonsoft.Json;
using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Queries
{
    /// <summary>
    /// Classe <c>AlarmQueries</c>
    /// <para>Classe responsável pelas operações de consulta de alarmes no IP21</para>
    /// </summary>
    public class AlarmQueries : IAlarmQueries
    {
        public readonly string _connectionString;
        private readonly CultureInfo cultureInfo = CultureInfo.GetCultureInfo("en");
        private readonly string datePattern = "dd-MMM-yy HH:mm:ss";
        private IMemoryCache _cache;
        public AlarmQueries(string connectionString, IMemoryCache cache)
        {
            _connectionString = connectionString;
            _cache = cache;
        }
       
        /// <summary>
        /// Método que consulta a base de dados do IP21 na área fixa e retorna as tags com alarme ativo
        /// </summary>
        /// <returns>Uma lista de alarmes encontrados.</returns>
        public List<Alarm> GetAlarms()
        {
            using(OdbcConnection db = new OdbcConnection(_connectionString))
            {
                db.ConnectionTimeout = 180;
                string queryAlarms = $"select * from v_sge_alarms";


                List<Entities.Alarm> alarmsResult = db.Query<Entities.Alarm>(queryAlarms, commandTimeout: 180).AsList();
                List<Entities.Alarms_Equipment> equipments = GetAlarmEquipments().ToList();

                alarmsResult.ForEach(x =>
                {
                    SetEquipments(x, equipments);
                });

                List<Alarm> alarms = alarmsResult.Where(x => x.EquipmentName != null).Select(alarm => Alarm.Load(alarm.VariableName, alarm.VariableDescription, alarm.EngUnit, alarm.Value, alarm.TimeStamp, alarm.EquipmentName, alarm.EquipmentDescription, alarm.AreaName, alarm.AreaDescription, alarm.SectionName, alarm.SectionDescription, alarm.SegmentName, alarm.SegmentDescription, alarm.Severity)).ToList();
                return alarms;
            }
        }
       
        /// <summary>
        ///  Método que consulta a base de dados do IP21 na área fixa e retorna as tags com alarme ativo e filtra pelo nível da hierarquia
        /// </summary>
        /// <param name="level">O nível da hierarquia, variando entre segmentName, areaName, sectionName e equipmentName.</param>
        /// <param name="name">O nome do nível selecionado.</param>
        /// <returns>>Uma lista de alarmes encontrados.</returns>
        public List<Alarm> GetAlarms(string level, string name)
        {
            using (OdbcConnection db = new OdbcConnection(_connectionString))
            {
                db.ConnectionTimeout = 180;
                string queryAlarms = $"select * from v_sge_alarms";


                List<Entities.Alarm> alarmsResult = db.Query<Entities.Alarm>(queryAlarms, commandTimeout: 180).AsList();
                List<Entities.Alarms_Equipment> equipments = GetAlarmEquipments().ToList();

                alarmsResult.ForEach(x =>
                {
                    SetEquipments(x, equipments);
                });

                List<Alarm> alarms = alarmsResult.Where(x => x.EquipmentName != null).Select(alarm => Alarm.Load(alarm.VariableName, alarm.VariableDescription, alarm.EngUnit, alarm.Value, alarm.TimeStamp, alarm.EquipmentName, alarm.EquipmentDescription, alarm.AreaName, alarm.AreaDescription, alarm.SectionName, alarm.SectionDescription, alarm.SegmentName, alarm.SegmentDescription, alarm.Severity)).ToList();
                alarms = FilterAlarmsByLevel(alarms, level, name);

                return alarms.OrderBy(x => x.AlarmSeverity).ThenByDescending(x => x.TimeStamp).ToList();
            }
        }
        /// <summary>
        /// Método que consulta a base de dados do IP21 na área fixa e retorna as tags com alarme crítico
        /// </summary>
        /// <returns>A lista de alarmes encontrados.</returns>
        public List<Alarm> GetCriticalAlarms()
        {
            using (OdbcConnection db = new OdbcConnection(_connectionString))
            {
                db.ConnectionTimeout = 180;

                string queryAlarms = "SELECT * FROM v_sge_alarms WHERE severity = 'CRITICO'";
                
                List<Entities.Alarm> alarmsResult = db.Query<Entities.Alarm>(queryAlarms, commandTimeout: 180).AsList();

                List<Entities.Alarms_Equipment> equipments = GetAlarmEquipments().ToList();

                alarmsResult.ForEach(x =>
                {
                    SetEquipments(x, equipments);
                });

                List<Alarm> alarms = alarmsResult.Where(x => x.EquipmentName != null).Select(alarm => Alarm.Load(alarm.VariableName, alarm.VariableDescription, alarm.EngUnit, alarm.Value, alarm.TimeStamp, alarm.EquipmentName, alarm.EquipmentDescription, alarm.AreaName, alarm.AreaDescription, alarm.SectionName, alarm.SectionDescription, alarm.SegmentName, alarm.SegmentDescription, alarm.Severity)).ToList();
                return alarms.OrderByDescending(x => x.TimeStamp).ToList();
            }
        }
       
        /// <summary>
        /// Método que pesquisa os valores das tags de alarm dentro do período passado.
        /// </summary>
        /// <param name="startTime">Data ínicio da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <returns>A lista de alarmes encontrados e seus respectivos timestamps.</returns>
        public List<Alarm> GetAlarmsTrend(DateTime startTime, DateTime endTime, string level, string levelName)
        {
            startTime = startTime.ToLocalTime();
            endTime = endTime.ToLocalTime();
            if (startTime > endTime)
            {
                throw new Application.ApplicationException(MessageUser.StartTimeGreaterThanEndTime());
            }
            if(endTime.ToLocalTime() > DateTime.Now)
            {
                throw new Application.ApplicationException(MessageUser.EndTimeGreaterThanCurrentDate());
            }

            using (OdbcConnection db = new OdbcConnection(_connectionString))
            {
                db.ConnectionTimeout = 180;
                //string query = $"SELECT * FROM V_SGE_AlarmsTrendDetail WHERE \"timestamp\" BETWEEN '{startTime.ToString("dd-MMM-yy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"))}' AND '{endTime.ToString("dd-MMM-yy HH:mm:ss", CultureInfo.GetCultureInfo("en-US"))}'";
                string query = $"SGE_SP_AlarmsTrend('{startTime.ToString(datePattern, cultureInfo)}', '{endTime.ToString(datePattern, cultureInfo)}')";

                List<Entities.Alarm> alarmsResult = db.Query<Entities.Alarm>(query, commandTimeout:1800).AsList();
                List<Entities.Alarms_Equipment> equipments = GetAlarmEquipments().ToList();

                alarmsResult.ForEach(x =>
                {
                    SetEquipments(x, equipments);
                });

                List<Alarm> alarms = alarmsResult.Where(x =>x.EquipmentName != null).Select(alarm => Alarm.Load(alarm.VariableName, alarm.VariableDescription, alarm.EngUnit, alarm.Value, alarm.TimeStamp, alarm.EquipmentName, alarm.EquipmentDescription, alarm.AreaName, alarm.AreaDescription, alarm.SectionName, alarm.SectionDescription, alarm.SegmentName, alarm.SegmentDescription, alarm.Severity)).ToList();
                List<IGrouping<string, Alarm>> alarmsByVariable = alarms.GroupBy(x => x.VariableName).ToList();
                alarms = alarmsByVariable.Select(x => x.OrderBy(x => x.AlarmSeverity).ThenByDescending(x => x.TimeStamp).FirstOrDefault()).ToList();

                alarms = FilterAlarmsByLevel(alarms, level, levelName, false);

                return alarms.OrderBy(x => x.AlarmSeverity).ThenByDescending(x => x.TimeStamp).ToList();
            }

        }
       
        /// <summary>
        /// Método responsável pela consulta e cache das variáveis ativas para cada equipamento na base de dados, para fazer a 
        /// atribuição do equipamento à variável e preencher as informações de localização: Segmento, Área e Seção que o Equipamento
        /// está.
        /// </summary>
        /// <returns>A lista de variáveis e seus respectivos equipamentos, com as
        /// informações de localização: Segmento, Área e Seção que o Equipamento.</returns>
        private IEnumerable<Entities.Alarms_Equipment> GetAlarmEquipments()
        {
            List<Entities.Alarms_Equipment> results;
            if (_cache.TryGetValue("_equipmentsList", out object alarmsEquipmentAux))
            {
                results = (List<Entities.Alarms_Equipment>)alarmsEquipmentAux;
            }
            else
            {
                using (IDbConnection db = new OdbcConnection(_connectionString))
                {
                    string query = "SELECT * FROM V_SGE_AlarmsFolder";
                    string querySections = "SELECT chave name, valor description from SGE_Sections where name <> ''";
                    string queryAreas = "SELECT chave name, valor description from SGE_Areas where name <> ''";
                    string querySegments = "SELECT chave name, valor description from SGE_Segments where name <> ''";

                    results = db.Query<Entities.Alarms_Equipment>(query, commandTimeout: 180).ToList();

                    var sections = db.Query<Entities.PlantLevel>(querySections, commandTimeout: 180).ToList();
                    var areas = db.Query<Entities.PlantLevel>(queryAreas, commandTimeout: 180).ToList();
                    var segments = db.Query<Entities.PlantLevel>(querySegments, commandTimeout: 180).ToList();

                    foreach(var equipment in results)
                    {
                        var section = sections.FirstOrDefault(s => equipment.EquipmentName.Contains(s.Name));
                        if (section != null)
                        {
                            equipment.SectionName = section.Name;
                            equipment.SectionDescription = section.Description;
                        }
                        
                        var area = areas.FirstOrDefault(a => equipment.EquipmentName.Contains(a.Name));
                        if(area != null)
                        {
                            equipment.AreaName = area.Name;
                            equipment.AreaDescription = area.Description;
                        }

                        var segment = segments.FirstOrDefault(seg => equipment.EquipmentName.Contains(seg.Name));
                        if (segment != null)
                        {
                            equipment.SegmentName = segment.Name;
                            equipment.SegmentDescription = segment.Description;
                        }
                    }

                    _cache.Set("_equipmentsList", results, new DateTimeOffset(DateTime.Today.AddDays(1)));
                }

            }

            //LxLogger.Info($"Equipments: {JsonConvert.SerializeObject(results.Select(x => new { x.EquipmentName, x.Record_name }))}");
            return results;
        }

        /// <summary>
        /// Método que filtra uma lista de alarmes por nível.
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem filtrados</param>
        /// <param name="level">O nível da hierarquia, variando entre segmentName, areaName, sectionName e equipmentName.</param>
        /// <param name="levelName">O nome do nível selecionado.</param>
        /// <returns>A Lista de alarmes filtrada.</returns>
        private List<Alarm> FilterAlarmsByLevel(List<Alarm> alarms, string level, string levelName, bool excludeNormal = true)
        {

            switch (level)
            {
                case "segmentName":
                    alarms = alarms.Where(x => x.SegmentName == levelName).ToList();
                    break;
                case "areaName":
                    alarms = alarms.Where(x => x.AreaName == levelName).ToList();
                    break;
                case "sectionName":
                    alarms = alarms.Where(x => x.SectionName == levelName).ToList();
                    break;
                case "equipmentName":
                    alarms = alarms.Where(x => x.EquipmentName == levelName).ToList();
                    excludeNormal = false;
                    break;
            }

            return excludeNormal ? alarms.Where(x => x.AlarmSeverity != AlarmSeverity.NORMAL).ToList() : alarms;
        }

        /// <summary>
        /// Filtra os alarmes por nível.
        /// </summary>
        /// <param name="alarms">Lista de alarmes a serem filtrados.</param>
        /// <param name="level">O nível a ser filtrado.</param>
        /// <param name="levelName">O nome do nível que servirá de filtro.</param>
        /// <returns>A lista de alarmes filtrados.</returns>
        private List<Alarm> FilterAlarmsByLevel(List<Alarm> alarms, Level level, string levelName)
        {

            switch (level)
            {
                case Level.Segment:
                    alarms = alarms.Where(x => x.SegmentName == levelName).ToList();
                    break;
                case Level.Area:
                    alarms = alarms.Where(x => x.AreaName == levelName).ToList();
                    break;
                case Level.Section:
                    alarms = alarms.Where(x => x.SectionName == levelName).ToList();
                    break;
                case Level.Equipment:
                    alarms = alarms.Where(x => x.EquipmentName == levelName).ToList();
                    break;
            }

            return alarms;
        }

        /// <summary>
        /// Método que atribui a variável do alarme ao equipamento.
        /// </summary>
        /// <param name="alarm">Alarme que terá o equipamento atribuído através do record_name.</param>
        /// <param name="equipments">Lista de nomes de equipamentos com as variáveis para atribuir.</param>
        private void SetEquipments(Entities.Alarm alarm, List<Entities.Alarms_Equipment> equipments)
        {
            Entities.Alarms_Equipment equipment = equipments.FirstOrDefault(e => e.Record_name == alarm.VariableName);
            if (equipment != null)
            {
                alarm.EquipmentName = equipment?.EquipmentName;
                alarm.EquipmentDescription = equipment.Description;
                alarm.SectionName = equipment.SectionName;
                alarm.SectionDescription = equipment.SectionDescription;
                alarm.SegmentName = equipment.SegmentName;
                alarm.SegmentDescription = equipment.SegmentDescription;
                alarm.AreaName = equipment.AreaName;
                alarm.AreaDescription = equipment.AreaDescription;
            }
        }
    }
}
