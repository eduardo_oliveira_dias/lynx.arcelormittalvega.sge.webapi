﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Domain.Indicators;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Text;
using Dapper;
using System.Linq;
using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System.Globalization;
using Lynx.ArcelorMittalVega.SGE.Domain.Graphics;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Queries
{
    /// <summary>
    /// Classe <c>IndicatorQueries</c>
    /// <para>Classe responsável pelas operações de consulta de dados de kpi's e indicadores.</para>
    /// </summary>
    public class IndicatorQueries : IIndicatorQueries
    {
        private readonly string _connectionString;

        public IndicatorQueries(string connectionString)
        {
            _connectionString = connectionString;
        }
        /// <summary>
        /// Método que consulta o valor do KPI atual do determinado ativo.
        /// </summary>
        /// <param name="itemName">Nome do ativo(Equipamento, Seção, Área ou Segmento)</param>
        /// <returns>O objeto com os dados de KPI, incluindo valor e timestamp.</returns>
        public Indicator GetKPI(string itemName)
        {
            using(IDbConnection db = new OdbcConnection(_connectionString))
            {
                string query = $"SELECT name, ip_input_val_critico value, ip_input_time \"timestamp\" from ip_kpiDef where name = 'KPI-{itemName}'";

                List<Entities.Indicator> indicatorsResult = db.Query<Entities.Indicator>(query, commandTimeout: 180).AsList();

                List<Indicator> indicators = indicatorsResult.Select(i => Indicator.Load(i.Name, i.Value, i.TimeStamp)).ToList();

                return indicators.FirstOrDefault();
            }
        }

        /// <summary>
        /// Método que consulta um ou mais valores de KPI dos ativos do nível selecionado dentro do período.
        /// </summary>
        /// <param name="startTime">Data início da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <param name="level">Nível da hierarquia para agrupar os kpi's.</param>
        /// <param name="levelName">Nome do nível para filtrar os kpi's.</param>
        /// <returns>A lista de objetos com os dados de KPI, incluindo valor e timestamp.</returns>
        public List<Indicator> GetKPI(DateTime startTime, DateTime endTime, Level level, string levelName)
        {
            startTime = startTime.ToLocalTime();
            endTime = endTime.ToLocalTime();
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {
                string query = $"SELECT * FROM V_SGE_KPI where level = {level} and \"timestamp\" BETWEEN {startTime.ToString("dd-MMM-yyyy HH:mm:ss", CultureInfo.GetCultureInfo("en"))} AND {endTime.ToString("dd-MM-yyyy HH:mm:ss", CultureInfo.GetCultureInfo("en"))}";

                List<Entities.Indicator> indicatorResult = db.Query<Entities.Indicator>(query, commandTimeout: 180).ToList();

                List<Indicator> indicators = indicatorResult.Select(x => Indicator.Load(x.Name, x.Value, x.TimeStamp, x.Level, x.LevelName, x.LevelDescription)).ToList();


                return indicators;
            }
        }

        /// <summary>
        /// Método que consulta um ou mais valores de KPI dos ativos do nível selecionado dentro do período, estruturando
        /// para apresentação no gráfico de tendência.
        /// </summary>
        /// <param name="startTime">Data início da pesquisa.</param>
        /// <param name="endTime">Data fim da pesquisa.</param>
        /// <param name="level">Nível da hierarquia para agrupar os kpi's.</param>
        /// <param name="levelName">Nome do nível para filtrar os kpi's.</param>
        /// <returns>A lista de objetos de gráfico de tendência com os dados de KPIs.</returns>
        public List<GraphicTrend> GetGraphicTrendKPI(DateTime startTime, DateTime endTime, Level level, string levelName)
        {
            startTime = startTime.ToLocalTime();
            endTime = endTime.ToLocalTime();
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {
                string query = $"SELECT * FROM V_SGE_KPI where level = {level} and \"timestamp\" BETWEEN {startTime.ToString("dd-MMM-yyyy HH:mm:ss", CultureInfo.GetCultureInfo("en"))} AND {endTime.ToString("dd-MM-yyyy HH:mm:ss", CultureInfo.GetCultureInfo("en"))}";

                List<Entities.Indicator> indicatorResult = db.Query<Entities.Indicator>(query, commandTimeout: 180).ToList();

                List<Indicator> indicators = indicatorResult.Select(x => Indicator.Load(x.Name, x.Value, x.TimeStamp, x.Level, x.LevelName, x.LevelDescription)).ToList();

                List<IGrouping<string, Indicator>> groups = indicators.GroupBy(x => x.LevelName).ToList();

                List<GraphicTrend> listGraphicTrend = new List<GraphicTrend>();

                foreach (var group in groups)
                {
                    GraphicTrend graphicTrend = new GraphicTrend();
                    graphicTrend.Info = group.Key;
                    graphicTrend.Description = group.Select(x => x.LevelDescription ?? x.LevelName).FirstOrDefault();

                    foreach(Indicator indicator in group)
                    {
                        GraphicDataItem item = new GraphicDataItem();
                        item.Value = item.Value;
                        item.Name = item.Name;

                        graphicTrend.Items.Add(item);
                    }

                    listGraphicTrend.Add(graphicTrend);
                }

                return listGraphicTrend;
            }
        }
    }
}
