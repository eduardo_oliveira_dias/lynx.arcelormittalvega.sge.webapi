﻿using Lynx.ArcelorMittalVega.SGE.Application.Queries;
using Lynx.ArcelorMittalVega.SGE.Domain;
using Lynx.ArcelorMittalVega.SGE.Domain.Equipments;
using Lynx.ArcelorMittalVega.SGE.Domain.Graphics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Text;
using System.Linq;
using Dapper;
using System.Globalization;
using Dapper;
using Lynx.ArcelorMittalVega.SGE.Domain.Alarms;
using Lynx.ArcelorMittalVega.SGE.Domain.Indicators;
using Lynx.ArcelorMittalVega.SGE.Domain.Reports;
using Microsoft.Extensions.Caching.Memory;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Queries
{
    /// <summary>
    /// Classe <c>ReportQueries</c>
    /// <para>Classe responsável pelas operações de consulta dos dados históricos do IP21 para geração de relatórios.</para>
    /// </summary>
    public class ReportQueries : IReportQueries
    {
        private readonly string _connectionString;
        private readonly CultureInfo cultureInfo = CultureInfo.GetCultureInfo("en");
        private readonly string datePattern = "dd-MMM-yy HH:mm:ss";
        private IMemoryCache _cache;
        private DateTimeOffset _expiredDateCache;
        public ReportQueries(string connectionString, IMemoryCache cache)
        {
            _connectionString = connectionString;
            _cache = cache;
            _expiredDateCache = new DateTimeOffset(DateTime.Today.AddDays(1));
        }

        /// <summary>
        /// Método responsável por filtrar os últimos alarmes status de cada equipamento, priorizando a severidade Crítica e após isso Alerta.
        /// </summary>
        /// <param name="filter">Objeto de filtro com a localização do equipamento, e a data desejada.</param>
        /// <returns>Lista de equipamentos com alarme no período.</returns>
        public List<Equipment> GetEquipmentStatus(AbstractReportFilter filter)
        {
            filter.StartTime = filter.StartTime.ToLocalTime();
            filter.EndTime = filter.EndTime.ToLocalTime();
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {
                //string query = $"select * from V_SGE_AlarmsTrendDetail WHERE \"TimeStamp\" BETWEEN '{filter.StartTime.ToString(datePattern, cultureInfo)}' AND '{filter.EndTime.ToString(datePattern, cultureInfo)}'";
                string query = $"SGE_SP_AlarmsTrend('{filter.StartTime.ToString(datePattern, cultureInfo)}', '{filter.EndTime.ToString(datePattern, cultureInfo)}')";
                List<Entities.Alarm> alarmsResult = db.Query<Entities.Alarm>(query, commandTimeout: 180).ToList();
                List<Entities.Alarms_Equipment> equipments = GetAlarmEquipments().Where(x => x.Level == "Equipment").ToList();

                alarmsResult.ForEach(x =>
                {
                    Entities.Alarms_Equipment equipment = equipments.FirstOrDefault(e => e.Record_name == x.VariableName);
                    if (equipment != null)
                    {
                        x.EquipmentName = equipment?.EquipmentName;
                        x.EquipmentDescription = equipment.Description;
                        x.SectionName = equipment.SectionName;
                        x.SectionDescription = equipment.SectionDescription;
                        x.SegmentName = equipment.SegmentName;
                        x.SegmentDescription = equipment.SegmentDescription;
                        x.AreaName = equipment.AreaName;
                        x.AreaDescription = equipment.AreaDescription;
                    }
                });
                
                List<Alarm> alarms = alarmsResult.Where(x => x.EquipmentName != null).Select(alarm => Alarm.Load(alarm.VariableName, alarm.VariableDescription, alarm.EngUnit, alarm.Value, alarm.TimeStamp, alarm.EquipmentName, alarm.EquipmentDescription, alarm.AreaName, alarm.AreaDescription, alarm.SectionName, alarm.SectionDescription, alarm.SegmentName, alarm.SegmentDescription, alarm.Severity)).ToList();
                List<IGrouping<string, Entities.Alarms_Equipment>> groupAlarms = equipments.GroupBy(x => x.EquipmentName).ToList();

                List<Equipment> equipmentsList = new List<Equipment>();

                foreach(var groupEquipment in groupAlarms)
                {
                    Alarm alarmSeverity = alarms.Where(x => x.EquipmentName == groupEquipment.Key).OrderBy(x => x.AlarmSeverity).ThenByDescending(x => x.TimeStamp).FirstOrDefault();
                    Entities.Alarms_Equipment alarmEquipment = groupEquipment.FirstOrDefault();

                    if (alarmSeverity != null) 
                    {
                        Equipment equipment = Equipment.Load(alarmEquipment.EquipmentName, alarmEquipment.Description, alarmSeverity?.Severity, alarmSeverity.TimeStamp);

                        equipment.SectionName = alarmEquipment.SectionName;
                        equipment.SectionDescription = alarmEquipment.SectionDescription;
                        equipment.SegmentName = alarmEquipment.SegmentName;
                        equipment.SegmentDescription = alarmEquipment.SegmentDescription;
                        equipment.AreaName = alarmEquipment.AreaName;
                        equipment.AreaDescription = alarmEquipment.AreaDescription;

                        equipment.AlertStatusPeriod = GetSeverityPeriod("ALERTA", alarms.Where(x => x.EquipmentName == equipment.Name).ToList(), filter.StartTime, filter.EndTime);
                        equipment.CriticalStatusPeriod = GetSeverityPeriod("CRITICO", alarms.Where(x => x.EquipmentName == equipment.Name).ToList(), filter.StartTime, filter.EndTime);
                        equipment.SeverityStatusDuration = TimeInLastStatus(alarms.Where(x => x.EquipmentName == equipment.Name).ToList(), filter.StartTime, filter.EndTime);
                        equipmentsList.Add(equipment);
                    }
                }

                equipmentsList = equipmentsList.Where(x => !String.IsNullOrEmpty(x.SeverityStatus)).ToList();
                equipmentsList = FilterEquipments(filter, equipmentsList);

                return equipmentsList.OrderBy(x => x.AlarmSeverity).ThenByDescending(x => x.TimeStampSeverityStatus).ToList();
            }
        }

        /// <summary>
        /// Método que consulta os alarmes no período e estrutura os dados em gráfico de barra.
        /// </summary>
        /// <param name="filter">Objeto de filtro com a localização do equipamento, e a data desejada.</param>
        /// <returns>Lista de alarmes agrupados por área para apresentação no gráfico de barra.</returns>
        public List<GraphicBar> GetGraphicBar(AbstractReportFilter filter)
        {
            filter.StartTime = filter.StartTime.ToLocalTime();
            filter.EndTime = filter.EndTime.ToLocalTime();
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {
                List<GraphicBar> listGraphics = new List<GraphicBar>();
                //string query = $"select * from V_SGE_AlarmsTrendDetail WHERE \"TimeStamp\" BETWEEN '{filter.StartTime.ToString(datePattern, cultureInfo)}' AND '{filter.EndTime.ToString(datePattern,cultureInfo)}'";
                string query = $"SGE_SP_AlarmsTrend('{filter.StartTime.ToString(datePattern, cultureInfo)}', '{filter.EndTime.ToString(datePattern, cultureInfo)}')";
                List<Entities.Alarm> alarmsResult = db.Query<Entities.Alarm>(query, commandTimeout: 180).ToList();
                List<Entities.Alarms_Equipment> equipments = GetAlarmEquipments().Where(x => x.Level == "Equipment").ToList();

                alarmsResult.ForEach(x =>
                {
                    Entities.Alarms_Equipment equipment = equipments.FirstOrDefault(e => e.Record_name == x.VariableName);
                    if(equipment != null)
                    {
                        x.EquipmentName = equipment?.EquipmentName;
                        x.EquipmentDescription = equipment.Description;
                        x.SectionName = equipment.SectionName;
                        x.SectionDescription = equipment.SectionDescription;
                        x.SegmentName = equipment.SegmentName;
                        x.SegmentDescription = equipment.SegmentDescription;
                        x.AreaName = equipment.AreaName;
                        x.AreaDescription = equipment.AreaDescription;
                    }
                });
                List<Alarm> alarms = alarmsResult.Where(x => x.EquipmentName != null).Select(alarm => Alarm.Load(alarm.VariableName, alarm.VariableDescription, alarm.EngUnit, alarm.Value, alarm.TimeStamp, alarm.EquipmentName, alarm.EquipmentDescription, alarm.AreaName, alarm.AreaDescription, alarm.SectionName, alarm.SectionDescription, alarm.SegmentName, alarm.SegmentDescription, alarm.Severity)).ToList();

                List<IGrouping<string, Alarm>> groups = alarms.GroupBy(x => x.EquipmentName).ToList();
                List<Equipment> equipmentsList = new List<Equipment>();

                foreach(var group in groups)
                {
                    Alarm alarm = group.OrderBy(x => x.AlarmSeverity).ThenByDescending(x => x.TimeStamp).FirstOrDefault();

                    Equipment equipment = Equipment.Load(alarm.EquipmentName, alarm.EquipmentDescription, alarm.Severity, alarm.TimeStamp);
                    equipment.SectionName = alarm.SectionName;
                    equipment.SectionDescription = alarm.SectionDescription;
                    equipment.SegmentName = alarm.SegmentName;
                    equipment.SegmentDescription = alarm.SegmentDescription;
                    equipment.AreaName = alarm.AreaName;
                    equipment.AreaDescription = alarm.AreaDescription;

                    equipmentsList.Add(equipment);
                }

                equipmentsList = FilterEquipments(filter, equipmentsList);

                List<IGrouping<string, Equipment>> groupsEquipment = equipmentsList.GroupBy(x => x.AreaName).ToList();
                foreach(IGrouping<string, Equipment> groupEquipment in groupsEquipment)
                {
                    GraphicBar graphicBar = new GraphicBar();
                    graphicBar.Info = groupEquipment.Key;
                    graphicBar.Description = equipmentsList.FirstOrDefault(x => x.AreaName == groupEquipment.Key).AreaDescription;

                    List<IGrouping<string, Equipment>> subGroupsEquipment = groupEquipment.GroupBy(x => x.SeverityStatus).ToList();

                    foreach(IGrouping<string, Equipment> subGroup in subGroupsEquipment)
                    {
                        GraphicDataItem item = new GraphicDataItem();
                        item.Name = subGroup.Key;
                        item.Value = subGroup.Count();

                        graphicBar.Items.Add(item);
                    }

                    listGraphics.Add(graphicBar);
                }

                
                
                return listGraphics;
            }
        }

        /// <summary>
        /// Método que consulta os KPIs das áreas, dentro do período, estruturando os dados para apresentação em gráfico de linha.
        /// </summary>
        /// <param name="filter">Objeto de filtro com a localização da data, e a data desejada.</param>
        /// <returns>Lista de kpis agrupados por área e timestamp para apresentação no gráfico de linha.</returns>
        public List<GraphicTrend> GetGraphicTrendKPI(AbstractReportFilter filter)
        {
            filter.StartTime = filter.StartTime.ToLocalTime();
            filter.EndTime = filter.EndTime.ToLocalTime();
            List<GraphicTrend> listGraphic = new List<GraphicTrend>();

            using (IDbConnection db = new OdbcConnection(_connectionString))
            {
                string query = $"SELECT * FROM V_SGE_KPI WHERE  \"LEVEL\" = 'Area' AND (\"TIMESTAMP\" BETWEEN '{filter.StartTime.ToString(datePattern, cultureInfo)}' AND '{filter.EndTime.ToString(datePattern, cultureInfo)}')";
                List<Entities.Indicator> indicatorsResult = db.Query<Entities.Indicator>(query, commandTimeout:180).ToList();
                List<Indicator> indicators = indicatorsResult.Select(x => Indicator.Load(x.Name, x.Value, x.TimeStamp, x.Level, x.LevelName, x.LevelDescription)).ToList();
                indicators = FilterIndicators(filter, indicators);

                List<IGrouping<string, Indicator>> groupIndicators = indicators.GroupBy(x => x.LevelName).ToList();

                foreach(IGrouping<string, Indicator> group in groupIndicators)
                {
                    GraphicTrend graphicTrend = new GraphicTrend();
                    graphicTrend.Info = group.Key;
                    graphicTrend.Description = indicators.FirstOrDefault(x => x.LevelName == group.Key).LevelDescription;

                    foreach(Indicator indicator in group)
                    {
                        GraphicDataItem item = new GraphicDataItem();
                        item.Name = indicator.TimeStamp.ToString("dd/MM/yyyy HH:mm:ss");
                        item.Value = indicator.Value;

                        graphicTrend.Items.Add(item);
                    }

                    listGraphic.Add(graphicTrend);
                }

                return listGraphic;
            }
        }

        /// <summary>
        /// Método que trás os dados agrupados em formato de relatório, para consulta única.
        /// </summary>
        /// <param name="filter">Objeto de filtro com a localização da data, e a data desejada.</param>
        /// <returns>O Objeto de relatório com a lista de equipamentos e os gráficos.</returns>
        public ReportEquipments GetReportEquipments(AbstractReportFilter filter)
        {
            ReportEquipments reportEquipments = new ReportEquipments();

            reportEquipments.GraphicKPIs = this.GetGraphicTrendKPI(filter);
            reportEquipments.GraphicEquipmentStatus = this.GetGraphicBar(filter);
            reportEquipments.EquipmentsStatus = this.GetEquipmentStatus(filter);

            return reportEquipments;
        }

        #region Auxiliares

        /// <summary>
        /// Método responsável pela consulta e cache das variáveis ativas para cada equipamento na base de dados, para fazer a 
        /// atribuição do equipamento à variável e preencher as informações de localização: Segmento, Área e Seção que o Equipamento
        /// está.
        /// </summary>
        /// <returns>A lista de variáveis e seus respectivos equipamentos, com as
        /// informações de localização: Segmento, Área e Seção que o Equipamento.</returns>
        private IEnumerable<Entities.Alarms_Equipment> GetAlarmEquipments()
        {
            List<Entities.Alarms_Equipment> results;
            if (_cache.TryGetValue("_equipmentsList", out object alarmsEquipmentAux))
            {
                results = (List<Entities.Alarms_Equipment>)alarmsEquipmentAux;
            }
            else
            {
                using (IDbConnection db = new OdbcConnection(_connectionString))
                {
                    string query = "SELECT * FROM V_SGE_AlarmsFolder";
                    string querySections = "SELECT chave name, valor description from SGE_Sections where name <> ''";
                    string queryAreas = "SELECT chave name, valor description from SGE_Areas where name <> ''";
                    string querySegments = "SELECT chave name, valor description from SGE_Segments where name <> ''";

                    results = db.Query<Entities.Alarms_Equipment>(query, commandTimeout: 180).ToList();

                    var sections = db.Query<Entities.PlantLevel>(querySections, commandTimeout: 180).ToList();
                    var areas = db.Query<Entities.PlantLevel>(queryAreas, commandTimeout: 180).ToList();
                    var segments = db.Query<Entities.PlantLevel>(querySegments, commandTimeout: 180).ToList();

                    foreach (var equipment in results)
                    {
                        var section = sections.FirstOrDefault(s => equipment.EquipmentName.Contains(s.Name));
                        if (section != null)
                        {
                            equipment.SectionName = section.Name;
                            equipment.SectionDescription = section.Description;
                        }

                        var area = areas.FirstOrDefault(a => equipment.EquipmentName.Contains(a.Name));
                        if (area != null)
                        {
                            equipment.AreaName = area.Name;
                            equipment.AreaDescription = area.Description;
                        }

                        var segment = segments.FirstOrDefault(seg => equipment.EquipmentName.Contains(seg.Name));
                        if (segment != null)
                        {
                            equipment.SegmentName = segment.Name;
                            equipment.SegmentDescription = segment.Description;
                        }
                    }

                    _cache.Set("_equipmentsList", results, new DateTimeOffset(DateTime.Today.AddDays(1)));
                }

            }

            return results;
        }

        /// <summary>
        /// Método que consulta o percentual de tempo que determinado equipamento ficou no status dentro período selecionado.
        /// </summary>
        /// <param name="severity">Severidade: CRITICO ou ALERTA</param>
        /// <param name="alarms">Lista de alarmes para verificação do tempo de cada severidade.</param>
        /// <param name="startTime">Data ínicio referência para cálculo de tempo</param>
        /// <param name="endTime">Data fim referência para cálculo de tempo</param>
        /// <returns>Valor flutuante com o Percentual calculado.</returns>
        private double GetSeverityPeriod(string severity, List<Alarm> alarms, DateTime startTime, DateTime endTime)
        {
            double percentPeriod = 0;
            TimeSpan timeSpanSeverityCount = TimeSpan.Zero;
            TimeSpan timeSpanTotal = endTime - startTime;
            alarms = alarms.OrderBy(x => x.TimeStamp).ToList();

            if(alarms.Count > 0)
            {
                for(int i = 0; i < alarms.Count; i++)
                {
                    if(alarms[i].Severity == severity)
                    {
                        if(i == (alarms.Count - 1)){//último alarme
                            timeSpanSeverityCount = timeSpanSeverityCount.Add(endTime - alarms[i].TimeStamp);
                        }
                        else
                        {
                            timeSpanSeverityCount = timeSpanSeverityCount.Add(alarms[i + 1].TimeStamp - alarms[i].TimeStamp);
                        }
                    }
                }

                percentPeriod = ((timeSpanSeverityCount.TotalSeconds * 100) / timeSpanTotal.TotalSeconds);

            }

            return percentPeriod;


        }

        /// <summary>
        /// Método que calcula o tempo que o equipamento está no status atual, dentro do período selecionado.
        /// </summary>
        /// <param name="alarms">Lista de alarmes para cálculo do tempo no último status.</param>
        /// <param name="startTime">Data ínicio referência para cálculo de tempo</param>
        /// <param name="endTime">Data fim referência para cálculo de tempo</param>
        /// <returns>O tempo que o equipamento está no status.</returns>
        private TimeSpan TimeInLastStatus(List<Alarm> alarms, DateTime startTime, DateTime endTime)
        {
            TimeSpan timeSpanInLastPeriod = TimeSpan.Zero;

            if (alarms.Count > 0)
            {
                alarms = alarms.OrderByDescending(x => x.TimeStamp).ToList();
                Alarm lastAlarm = alarms.FirstOrDefault();
                
                timeSpanInLastPeriod = endTime - lastAlarm.TimeStamp;
                
                bool differentSeverity = false; int count = 1;
                while (!differentSeverity)
                {
                    if(count < alarms.Count)
                    {
                        Alarm alarm = alarms[count];

                        if(alarm.Severity != lastAlarm.Severity)
                        {
                            differentSeverity = true;
                            break;
                        }
                        else
                        {
                            lastAlarm = count >= 1 ? alarms[count - 1] : alarms[0];
                            timeSpanInLastPeriod = timeSpanInLastPeriod.Add(lastAlarm.TimeStamp - alarm.TimeStamp);
                        }

                    }
                    else
                    {
                        timeSpanInLastPeriod = endTime - startTime;
                        differentSeverity = true;
                        break;
                    }

                    count++;
                }

            }

            return timeSpanInLastPeriod;


        }

        /// <summary>
        /// Método que filtra os equipamentos conforme o segmento, área e seção.
        /// </summary>
        /// <param name="filter">Objeto de filtro com a localização do equipamento.</param>
        /// <param name="equipmentList">Lista de equipamentos a ser filtrada.</param>
        /// <returns>Lista de equipamentos filtrada.</returns>
        private List<Equipment> FilterEquipments(AbstractReportFilter filter, List<Equipment> equipmentList)
        {
            if (!String.IsNullOrEmpty(filter.SegmentName))
            {
                equipmentList = equipmentList.Where(x => x.SegmentName == filter.SegmentName).ToList();
            }

            if (!String.IsNullOrEmpty(filter.AreaName))
            {
                equipmentList = equipmentList.Where(x => x.AreaName == filter.AreaName).ToList();
            }

            if (!String.IsNullOrEmpty(filter.SectionName))
            {
                equipmentList = equipmentList.Where(x => x.SectionName == filter.SectionName).ToList();
            }

            return equipmentList;
        }

        /// <summary>
        /// Método que filtra os kpi's conforme o segmento, área e seção do equipamento desse kpi.
        /// </summary>
        /// <param name="filter">Objeto de filtro com a localização do equipamento do kpi.</param>
        /// <param name="equipmentList">Lista de kpi's a ser filtrada.</param>
        /// <returns>Lista de kpi's filtrada.</returns>
        private List<Indicator> FilterIndicators(AbstractReportFilter filter, List<Indicator> indicatorList)
        {
            if (!String.IsNullOrEmpty(filter.AreaName))
            {
                indicatorList = indicatorList.Where(x => x.LevelName == filter.AreaName).ToList();
            }

            return indicatorList;
        }
        #endregion
    }
}
