﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Entities
{
    public class SystemConfiguration
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
