﻿using Lynx.ArcelorMittalVega.SGE.Domain.PlantLevels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Entities
{
    public class Indicator
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public DateTime TimeStamp { get;set; }
        public Level Level { get; set; }
        public string LevelName { get; set; }
        public string LevelDescription { get; set; }
    }
}
