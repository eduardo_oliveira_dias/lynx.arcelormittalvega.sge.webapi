﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Entities
{
    public class Variable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string IPPlantArea { get; set; }
        public string EngUnits { get; set; }
        public object Value { get; set; }
        public object Quality { get; set; }
        public object IP_High_High_Limit { get; set; }
        public object IP_High_Limit { get; set; }
        public object IP_Low_Low_Limit { get; set; }
        public object IP_Low_Limit { get; set; }
        public object IP_Graph_Maximum { get; set; }
        public object IP_Graph_Minimum { get; set; }
        public Equipment Equipment { get; set; }
        public string EquipmentName { get; set; }
        public string EquipmentDescription { get; set; }
        public string Severity { get; set; }
        
    }
}
