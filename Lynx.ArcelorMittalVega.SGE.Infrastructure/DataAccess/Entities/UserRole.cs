﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Entities
{
    public class UserRole
    {
        public string RoleName { get; set; }
        public string UserName { get; set; }
    }
}
