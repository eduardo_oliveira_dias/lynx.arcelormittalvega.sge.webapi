﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Entities
{
    public class PlantLevel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Level { get; set; }
        public PlantLevel Parent { get; set; }
        public List<PlantLevel> Children { get; set; }
        public List<Indicator> Indicators { get; set; } = new List<Indicator>();
        public List<Alarm> Alarms { get; set; } = new List<Alarm>();
        public int AlarmCount { get; set; }
        public string ParentName { get; set; }

    }
}
