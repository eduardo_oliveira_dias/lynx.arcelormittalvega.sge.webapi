﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Entities.Enums
{
    public enum AlarmSeverity
    {
        CRITICAL,
        ALERT,
        NORMAL
    }
}
