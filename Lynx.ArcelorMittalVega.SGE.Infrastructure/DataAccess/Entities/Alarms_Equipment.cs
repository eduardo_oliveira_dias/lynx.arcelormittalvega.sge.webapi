﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Entities
{
    public class Alarms_Equipment
    {
        public string Record_name { get; set; }
        public string EquipmentName { get; set; }
        public string Description { get; set; }
        public string Folder_Parent { get; set; }
        public string SectionName { get; set; }
        public string SectionDescription { get; set; }
        public string AreaName { get; set; }
        public string AreaDescription { get; set; }
        public string SegmentName { get; set; }
        public string SegmentDescription { get; set; }
        public string Path { get; set; }
        public string DescriptionPath { get; set; }
        public string Level { get; set; }
    }
}
