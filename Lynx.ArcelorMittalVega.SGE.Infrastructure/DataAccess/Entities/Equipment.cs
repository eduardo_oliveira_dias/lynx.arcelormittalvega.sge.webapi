﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Entities
{
    public class Equipment : PlantLevel
    {
        public PlantLevel Area { get; set; }
        public List<Variable> Variables { get; set; }
    }
}
