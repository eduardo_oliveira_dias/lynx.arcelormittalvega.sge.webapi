﻿using Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Entities
{
    public class Alarm
    {
        public string VariableName { get; set; }
        public string VariableDescription { get; set; }
        public string EngUnit { get; set; }
        public object Value { get; set; }
        public string Path { get; set; }
        public string DescriptionPath { get; set; }
        public DateTime TimeStamp { get; set; }
        public string EquipmentName { get; set; }
        public string EquipmentDescription { get; set; }
        public string AreaName { get; set; }
        public string AreaDescription { get; set; }
        public string SectionName { get; set; }
        public string SectionDescription { get; set; }
        public string SegmentName { get; set; }
        public string SegmentDescription { get; set; }
        //public PlantLevel Equipment { get; set; }
        //public PlantLevel Area { get; set; }
        public string Severity { get; set; }

    }
}
