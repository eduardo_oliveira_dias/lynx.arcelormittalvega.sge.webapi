﻿using Autofac;
using System;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess
{
    public class Module : Autofac.Module
    {
        public string ConnectionString { get; set; }
        public Module(string connectionString)
        {
            ConnectionString = connectionString;
        }
        /// <summary>
        /// Registra as classes e interfaces de queries e repositórios, para serem usadas como dependência.
        /// </summary>
        /// <param name="builder">Objeto responsável por fazer o registro das classes.</param>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(InfrastructureException).Assembly)
                .WithParameter("connectionString", ConnectionString)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
