﻿using Lynx.ArcelorMittalVega.SGE.Application.Repositories;
using Lynx.ArcelorMittalVega.SGE.Domain.SystemConfigurations;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Text;
using Dapper;
using System.Linq;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Repositories
{
    /// <summary>
    /// Classe <c>ConfigurationRepository</c>
    /// <para>Classe responsável pelas operações de leitura e escrita das configurações do sistema persistindo no IP21.</para>
    /// </summary>
    public class ConfigurationRepository : IConfigurationReadOnlyRepository, IConfigurationWriteOnlyRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public ConfigurationRepository(IConfiguration configuration, string connectionString)
        {
            _configuration = configuration;
            _connectionString = connectionString;
        }

        /// <summary>
        /// Método que adiciona uma um parâmetro para ser visualizada e gerenciada pelo sistema.
        /// </summary>
        /// <param name="variableParameter">Um objeto de chave-valor com o nome do parâmetro e o nome da variável a ser inserida.</param>
        public void AddVariableParameter(SystemConfiguration variableParameter)
        {
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {
                string command = $" INSERT INTO SGE_VariableParameters(CHAVE, VALOR) VALUES('{variableParameter.Key}','{variableParameter.Value}')";

                var row = db.Execute(command);

                return;
            }
        }
        /// <summary>
        /// Método que deleta um parâmetro da visualização do sistema. O parâmetro não é removido da base de dados.
        /// </summary>
        /// <param name="key">O nome do parâmetro a ser removido.</param>
        public void DeleteVariableParameter(string key)
        {
            using(IDbConnection db = new OdbcConnection(_connectionString))
            {
                string command = $" DELETE FROM SGE_VariableParameters where CHAVE = '{key}'";

                var row = db.Execute(command);

                return;
            }
        }

        /// <summary>
        /// Retorna o valor de determinada configuração.
        /// </summary>
        /// <param name="key">Chave da configuração para busca.</param>
        /// <returns>Valor da variável.</returns>
        public object Get(string key)
        {
            //var appSettings = _configuration.GetSection();
            Dictionary<string, object> config = new Dictionary<string, object>();
            config = _configuration.GetSection(key).Get<Dictionary<string, object>>();
            

            return config;
        }
        /// <summary>
        /// Método que consulta as configurações gerais da aplicação.
        /// </summary>
        /// <returns>Lista de objetos chave-valor com as configuraões cadastradas.</returns>
        public List<SystemConfiguration> GetGeneralConfigurations()
        {
            using(IDbConnection db = new OdbcConnection(_connectionString))
            {
                string query = "SELECT CHAVE \"Key\", VALOR Value from SGE_Config";

                List<Entities.SystemConfiguration> configResults = db.Query<Entities.SystemConfiguration>(query, commandTimeout: 180).ToList();

                List<SystemConfiguration> systemConfigurations = configResults.Select(c => SystemConfiguration.Load(c.Key, c.Value)).ToList();

                return systemConfigurations;
            }
        }

        /// <summary>
        /// Método responsável por consultar o posicionamento dos gráficos dos elementos de cada nível
        /// </summary>
        /// <returns>Lista de objetos chave-valor com o posicionamento x e y de cada elemento.</returns>
        public List<SystemConfiguration> GetObjectPositions()
        {
            using(IDbConnection db = new OdbcConnection(_connectionString))
            {
                string query = "SELECT CHAVE \"Key\", VALOR Value from SGE_ItemPosition";
                
                List<Entities.SystemConfiguration> configResults = db.Query<Entities.SystemConfiguration>(query, commandTimeout: 180).ToList();

                List<SystemConfiguration> systemConfigurations = configResults.Select(c => SystemConfiguration.Load(c.Key, c.Value)).ToList();

                return systemConfigurations;
            }
        }

        /// <summary>
        /// Método que retorna a lista dos parametros de variável a serem apresentados na tela de equipamentos
        /// </summary>
        /// <returns>Lista de objetos chave-valor com os nomes dos parâmetros das variáveis</returns>
        public List<SystemConfiguration> GetVariableParameters()
        {
            using (IDbConnection db = new OdbcConnection(_connectionString))
            {
                string query = "SELECT CHAVE \"Key\", VALOR Value from SGE_VariableParameters where CHAVE IS NOT NULL";

                List<Entities.SystemConfiguration> configResults = db.Query<Entities.SystemConfiguration>(query, commandTimeout: 180).ToList();

                List<SystemConfiguration> systemConfigurations = configResults.Select(c => SystemConfiguration.Load(c.Key, c.Value)).ToList();

                return systemConfigurations;
            }
        }

        /// <summary>
        /// Método que atualiza as configurações salvas em batch.
        /// </summary>
        /// <param name="systemConfigurations">Lista de objetos com o valor a serem atualizados.</param>
        public void UpdateGeneralConfigurations(List<SystemConfiguration> systemConfigurations)
        {
            using(IDbConnection db = new OdbcConnection(_connectionString))
            {
                foreach(var configuration in systemConfigurations)
                {
                    string command = $"UPDATE SGE_Config set VALOR = '{configuration.Value}' WHERE CHAVE = '{configuration.Key}'";
                    var row = db.Execute(command);

                }

                return;
            }
        }
    }
}
