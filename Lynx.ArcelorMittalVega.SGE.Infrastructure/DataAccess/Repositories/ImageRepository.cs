﻿using Lynx.ArcelorMittalVega.SGE.Application.Repositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Repositories
{
    /// <summary>
    /// Classe <c>ImageRepository</c>
    /// <para>Classe responsável pelas operações de leitura e escrita das imagens do sistema.</para>
    /// </summary>
    public class ImageRepository : IImageWriteOnlyRepository, IImageReadOnlyRepository
    {
        [Obsolete]
        public string GetPath(string name)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Método Responsável de salvar a nova imagem no servidor.
        /// </summary>
        /// <param name="file">O arquivo de imagem a ser salva.</param>
        /// <param name="path">O caminho a ser salvo.</param>
        /// <param name="filename">O nome do arquivo.</param>
        /// <returns>O nome do arquivo salvo.</returns>
        public string Upload(IFormFile file, string path, string filename)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);

                }
                using (FileStream fileStream = System.IO.File.Create(path + filename))
                {
                    file.CopyTo(fileStream);
                    fileStream.Flush();
                    string extensionFile = Path.GetExtension(path + filename);
                    if(extensionFile == ".jpg" && File.Exists(path+filename.Replace(".jpg", ".png"))){
                        File.Delete(path + filename.Replace(".jpg", ".png"));
                    }else if (extensionFile == ".png" && File.Exists(path + filename.Replace(".png", ".jpg")))
                    {
                        File.Delete(path + filename.Replace(".png", ".jpg"));
                    }
                    return filename;
                }
            }
            catch (Exception) {
                return null;
            }
        }

    }
}
