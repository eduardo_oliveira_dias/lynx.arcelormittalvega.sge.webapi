﻿using Lynx.ArcelorMittalVega.SGE.Application.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using System.Data;
using System.Data.Odbc;
using Lynx.ArcelorMittalVega.SGE.Application.Util;

namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Repositories
{
    /// <summary>
    /// Classe <c>VariableRepository</c>
    /// <para>Classe responsável pelas operações de leitura e escrita de valores de variáveis.</para>
    /// </summary>
    public class VariableRepository : IVariableWriteOnlyRepository, IVariableReadOnlyRepository
    {
        private string _connectionString;

        public VariableRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// Método que atualiza o valor do parâmetro da variável.
        /// </summary>
        /// <param name="variableName">Nome da variável.</param>
        /// <param name="parameter">Nome do parâmetro.</param>
        /// <param name="value">Valor do parâmetro.</param>
        public void Update(string variableName, string parameter, object value)
        {
            using(IDbConnection db = new OdbcConnection(_connectionString))
            {
                string command = $"UPDATE ip_analogDef set {parameter} = '{value}' where name like '{variableName}%';";

                int rows = db.Execute(command);

                if(rows < 1)
                {
                    throw new Application.ApplicationException(MessageUser.SaveFailed(parameter));
                }
            }
        }
    }
}
