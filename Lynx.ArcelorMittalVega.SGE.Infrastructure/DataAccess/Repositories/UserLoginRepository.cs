﻿using Dapper;
using Lynx.ArcelorMittalVega.SGE.Application.Repositories;
using Lynx.ArcelorMittalVega.SGE.Domain.Users;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;


namespace Lynx.ArcelorMittalVega.SGE.Infrastructure.DataAccess.Repositories
{
    /// <summary>
    /// Classe <c>UserLoginRepository</c>
    /// <para>Classe responsável pelas operações de leitura de usuários e roles do Ip21</para>
    /// </summary>
    public class UserLoginRepository : IUserReadOnlyRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public UserLoginRepository(IConfiguration configuration, string connectionString)
        {
            _configuration = configuration;
            _connectionString = connectionString;
        }
        /// <summary>
        /// Método responsável por validar o usuário e senha da instância de usuário 
        /// </summary>
        /// <param name="user">Usuário a ser validado, serão utilizados os campos <value>Name</value> e <value>Password</value> da classe: <c>UserAD</c> </param>
        /// <returns>True se o login for bem sucedido, False se o usuário ou senha estiverem incorretos</returns>
        public bool Login(UserAD user)
        {
            using(PrincipalContext pc = new PrincipalContext(ContextType.Domain, _configuration["Domain"]))
            {
                var validCredentials = pc.ValidateCredentials(user.Name, user.Password);

                return validCredentials;
            }
        }
        /// <summary>
        /// Método responsável por consultar quais as "Roles" do Ip21 que o usuário é membro.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Um array(<c>string[]</c>) com os nomes das Roles do Usuário.</returns>
        public string[] GetUserRoles(UserAD user)
        {
            using(IDbConnection db = new OdbcConnection(_connectionString))
            {
                string query = "SGE_UserRoles";

                List<Entities.UserRole> userRoles = db.Query<Entities.UserRole>(query, commandTimeout: 180).ToList();

                userRoles = userRoles.Where(x => x.UserName.Contains(user.SamAccountName)).ToList();

                return userRoles.Select(x => x.RoleName).ToArray();
            }
        }
        /// <summary>
        /// Método responsável por recuperar os dados de usuário do domínio especificado no parâmetro "Domain", configurado no appsettings.json
        /// ***Esse método não está funcionando nas redes amnet.local e amauto.local
        /// </summary>
        /// <param name="user">Usuário a ser consultado no domínio</param>
        /// <returns></returns>
        [Obsolete]
        public UserAD GetUserInfo(UserAD user)
        {
            try
            {
                PrincipalContext context = new PrincipalContext(ContextType.Domain, _configuration["Domain"]);
                UserPrincipal principal = new UserPrincipal(context);

                if (context != null)
                {
                    principal = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, user.Name+"@"+_configuration["domain"]);
                }

                return UserAD.FromUserPrincipal(principal);

            }catch(Exception e)
            {
                throw new Exception($"Erro ao logar com o usuário: {user.Name}", e);
            }
        }
    }
}
